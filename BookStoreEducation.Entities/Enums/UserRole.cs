﻿namespace BookStoreEducation.Entities.Enums
{
    public enum UserRole
    {
        None = 0,
        Client = 1,
        Admin = 2
    }
}
