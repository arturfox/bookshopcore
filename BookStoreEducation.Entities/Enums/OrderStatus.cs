﻿namespace BookStoreEducation.Entities.Enums
{
    public enum OrderStatus
    {
        None = 0,
        Unpaid = 1,
        Paid = 2
    }
}
