﻿using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("PrintingEditions")]
    public class PrintingEdition : BaseEntity
    {
        public string Description { get; set; }
        public string Title { get; set; }
        public string NormalizedTitle { get; set; }
        public PrintingEditionType PrintingEditionType { get; set; }
        public Currency Currency { get; set; }
        public double Price { get; set; }

        public long? ImageSourceId { get; set; }

        [ForeignKey(nameof(ImageSourceId))]
        public virtual ImageSource ImageSource { get; set; }

        public ICollection<Author> Authors { get; set; }
        public List<AuthorsInPrintingEdition> AuthorsInPrintingEditions { get; set; }

        public ICollection<Cart> Carts { get; set; }
        public List<CartPrintingEdition> CartPrintingEditions { get; set; }

        public PrintingEdition() : base()
        {
            NormalizedTitle = Title?.ToUpper();
        }
    }
}
