﻿using System;

namespace BookStoreEducation.Entities.Entities.Interfaces
{
    public interface IBaseEntity
    {
        long Id { get; set; } 
        DateTime CreationDate { get; set; }
        bool IsRemoved { get; set; }
    }
}
