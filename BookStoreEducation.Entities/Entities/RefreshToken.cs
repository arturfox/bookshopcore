﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("RefreshTokens")]
    public class RefreshToken : BaseEntity
    {
        public string Token { get; set; }
        public string AccessTokenPrint { get; set; }
        public DateTime Expires { get; set; }
        public DateTime? Revoked { get; set; }
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual ApplicationUser User { get; set; }
    }
}
