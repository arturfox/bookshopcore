﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using BookStoreEducation.Entities.Entities.Interfaces;

namespace BookStoreEducation.Entities.Entities
{
	public abstract class BaseEntity : IBaseEntity
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

		public DateTime CreationDate { get; set; }
        public bool IsRemoved { get; set; }

        public BaseEntity()
		{
			CreationDate = DateTime.UtcNow;
		}
	}
}
