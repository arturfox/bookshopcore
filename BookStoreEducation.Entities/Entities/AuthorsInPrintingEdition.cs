﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("AuthorsInPrintingEditions")]
    public class AuthorsInPrintingEdition : BaseEntity
    {
        public long AuthorId { get; set; }
        public long PrintingEditionId { get; set; }

        public virtual Author Author { get; set; }
        public virtual PrintingEdition PrintingEdition { get; set; }
    }
}
