﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("CartPrintingEditions")]
    public class CartPrintingEdition : BaseEntity
    {
        public int Quantity { get; set; }

        public long CartId { get; set; }
        public long PrintingEditionId { get; set; }

        public virtual Cart Cart { get; set; }
        public virtual PrintingEdition PrintingEdition { get; set; }
    }
}
