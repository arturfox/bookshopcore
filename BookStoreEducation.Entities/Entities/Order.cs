﻿using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("Orders")]
    public class Order : BaseEntity
    {
        public string Description { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public long UserId { get; set; }
        public long? PaymentId { get; set; }

        public ICollection<OrderPosition> OrderPositions { get; set; }
        [ForeignKey(nameof(UserId))]
        public virtual ApplicationUser User { get; set; }
        [ForeignKey(nameof(PaymentId))]
        public virtual Payment Payment { get; set; }
    }
}
