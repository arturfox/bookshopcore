﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("Authors")]
    public class Author : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string NormalizedFirstName { get; set; }
        public string NormalizedLastName { get; set; }

        public Author() : base()
        {
            if (FirstName != null)
            {
                NormalizedFirstName = FirstName.ToUpper();
            }
            if (LastName != null)
            {
                NormalizedLastName = LastName.ToUpper();
            }
        }

        public ICollection<PrintingEdition> PrintingEditions { get; set; }
        public List<AuthorsInPrintingEdition> AuthorsInPrintingEditions { get; set; }
    }
}
