﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("OrderPositions")]
    public class OrderPosition : BaseEntity
    {
        public int Count { get; set; }
        public long OrderId { get; set; }
        public long PrintingEditionId { get; set; }

        [ForeignKey(nameof(OrderId))]
        public virtual Order Order { get; set; }

        [ForeignKey(nameof(PrintingEditionId))]
        public virtual PrintingEdition PrintingEdition { get; set; }
    }
}
