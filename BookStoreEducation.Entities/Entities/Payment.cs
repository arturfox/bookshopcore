﻿using BookStoreEducation.Entities.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("Payments")]
    public class Payment : BaseEntity
    {
        public string TransactionId { get; set; }
        public bool IsSucceed { get; set; }
        public Currency? Currency { get; set; }
        public double? Amount { get; set; }
    }
}
