﻿using BookStoreEducation.Entities.Entities.Interfaces;
using BookStoreEducation.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    public class ApplicationUser : IdentityUser<long>, IBaseEntity
    {
        public DateTime CreationDate { get; set; }
        public bool IsRemoved { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NormalizedFirstName { get; set; }
        public string NormalizedLastName { get; set; }
        public AccountStatus AccountStatus { get; set; }

        public long? ImageSourceId { get; set; }

        [ForeignKey(nameof(ImageSourceId))]
        public virtual ImageSource ImageSource { get; set; }

        public ApplicationUser()
        {
            CreationDate = DateTime.UtcNow;
            NormalizedFirstName = FirstName?.ToUpper();
            NormalizedLastName = LastName?.ToUpper();
        }
    }
}
