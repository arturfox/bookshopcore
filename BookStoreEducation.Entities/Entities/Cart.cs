﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("Carts")]
    public class Cart : BaseEntity
    {
        public long UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual ApplicationUser User { get; set; }

        public ICollection<PrintingEdition> PrintingEditions { get; set; }
        public List<CartPrintingEdition> CartPrintingEditions { get; set; }
    }
}
