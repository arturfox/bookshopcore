﻿using System.ComponentModel.DataAnnotations.Schema;

namespace BookStoreEducation.Entities.Entities
{
    [Table("ImageSources")]
    public class ImageSource : BaseEntity
    {
        public string Key { get; set; }
        public string Region { get; set; }
        public string BucketName { get; set; }
        public string ObjectUrl => $"https://{BucketName}.s3.{Region}.amazonaws.com/{Key}";
    }
}
