﻿namespace BookStoreEducation.BusinessLogicLayer.Configurations
{
    public class SmtpOptions
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
        public string Host { get; set; }
        public bool UseSsl { get; set; }
        public string DisplayName { get; set; }
    }
}
