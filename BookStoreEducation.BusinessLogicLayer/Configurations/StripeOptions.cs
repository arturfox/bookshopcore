﻿namespace BookStoreEducation.BusinessLogicLayer.Configurations
{
    public class StripeOptions
    {
        public string SecretKey { get; set; }
        public string WebHookSecretKey { get; set; }
        public string SuccessFrontendRoute { get; set; }
        public string CancelFrontendRoute { get; set; }
    }
}
