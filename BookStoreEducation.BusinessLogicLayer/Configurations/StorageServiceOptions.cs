﻿namespace BookStoreEducation.BusinessLogicLayer.Configurations
{
    public class StorageServiceOptions
    {
        public string AccessKeyId { get; set; }
        public string SecretAccessKey { get; set; }
        public string BucketName { get; set; }
        public string Region { get; set; }
    }
}
