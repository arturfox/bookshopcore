﻿namespace BookStoreEducation.BusinessLogicLayer.Configurations
{
    public class AppSettings
    {
        public string BackendUrl { get; set; }
        public string FrontedUrl { get; set; }
    }
}
