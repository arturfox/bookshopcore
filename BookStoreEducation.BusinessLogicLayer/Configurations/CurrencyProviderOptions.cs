﻿namespace BookStoreEducation.BusinessLogicLayer.Configurations
{
    public class CurrencyProviderOptions
    {
        public string Url { get; set; }
    }
}
