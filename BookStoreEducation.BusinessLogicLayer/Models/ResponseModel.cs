﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models
{
    public class ResponseModel
    {
        public ResponseModel()
        {
            Errors = new List<string>();
        }

        [JsonProperty(PropertyName = "isSucceed")]
        public bool IsSucceed { get; set; }

        [JsonProperty(PropertyName = "statusCode")]
        public int StatusCode { get; set; }

        [JsonProperty(PropertyName = "errors")]
        public List<string> Errors { get; set; }
    }

    public class ResponseModel<TResult> : ResponseModel
    {
        [JsonProperty(PropertyName = "data")]
        public TResult Data { get; set; }
    }
}
