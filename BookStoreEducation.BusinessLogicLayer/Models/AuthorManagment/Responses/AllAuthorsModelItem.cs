﻿namespace BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Responses
{
    public class AllAuthorsModelItem
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
