﻿using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Responses
{
    public class AuthorsModelItem
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public IEnumerable<PrintingEditionModel> PrintingEditions { get; set; }

        public AuthorsModelItem()
        {
            PrintingEditions = new List<PrintingEditionModel>();
        }
    }
}
