﻿using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Responses
{
    public class AllAuthorsModel
    {
        public IEnumerable<AllAuthorsModelItem> Items { get; set; }

        public AllAuthorsModel()
        {
            Items = new List<AllAuthorsModelItem>();
        }
    }
}
