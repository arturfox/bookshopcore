﻿using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Responses
{
    public class AuthorsModel
    {
        public IEnumerable<AuthorsModelItem> Items { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalItems { get; set; }

        public AuthorsModel()
        {
            Items = new List<AuthorsModelItem>();
        }
    }
}
