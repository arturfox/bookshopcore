﻿namespace BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Responses
{
    public class PrintingEditionModel
    {
        public string Title { get; set; }
    }
}
