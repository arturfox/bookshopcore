﻿using System.ComponentModel.DataAnnotations;

namespace BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Requests
{
    public class AddAuthorModel
    {
        [MinLength(3), MaxLength(20)]
        public string FirstName { get; set; }
        [MinLength(3), MaxLength(20)]
        public string LastName { get; set; }
    }
}
