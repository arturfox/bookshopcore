﻿using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Requests
{
    public class UpdateCatalogItemModel
    {
        public long Id { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public PrintingEditionType PrintingEditionType { get; set; }
        public double Price { get; set; }
        public Currency Currency { get; set; }
        public string ImageSource { get; set; }
        public IEnumerable<long> AuthorsIds { get; set; }

        public UpdateCatalogItemModel()
        {
            AuthorsIds = new List<long>();
        }
    }
}
