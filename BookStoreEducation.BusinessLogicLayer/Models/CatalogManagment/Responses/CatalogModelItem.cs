﻿using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Responses
{
    public class CatalogModelItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public Currency Currency { get; set; }
        public string ImageSource { get; set; }
        public IEnumerable<AuthorModel> Authors { get; set; }
        public PrintingEditionType PrintingEditionType { get; set; }

        public CatalogModelItem()
        {
            Authors = new List<AuthorModel>();
        }
    }
}
