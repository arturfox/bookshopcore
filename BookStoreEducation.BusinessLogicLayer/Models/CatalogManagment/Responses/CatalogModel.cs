﻿using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Responses
{
    public class CatalogModel
    {
        public IEnumerable<CatalogModelItem> Items { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalItems { get; set; }

        public CatalogModel()
        {
            Items = new List<CatalogModelItem>();
        }
    }
}
