﻿namespace BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Responses
{
    public class AuthorModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
