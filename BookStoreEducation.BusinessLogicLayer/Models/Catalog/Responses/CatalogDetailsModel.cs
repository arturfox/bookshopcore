﻿using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.Catalog.Responses
{
    public class CatalogDetailsModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public Currency Currency { get; set; }
        public string ImageSource { get; set; }
        public IEnumerable<AuthorModel> Authors { get; set; }
        public PrintingEditionType PrintingEditionType { get; set; }

        public CatalogDetailsModel()
        {
            Authors = new List<AuthorModel>();
        }
    }  
}
