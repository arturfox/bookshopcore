﻿using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace BookStoreEducation.BusinessLogicLayer.Models.Catalog.Responses
{
    public class CatalogModelItem
    {
        public long Id { get; set; }
        public string Title { get; set; }
        [JsonIgnore]
        public double BasePrice { get; set; }
        [JsonIgnore]
        public Currency BaseCurrency { get; set; }
        public double Price { get; set; }
        public Currency Currency { get; set; }
        public string ImageSource { get; set; }
        public IEnumerable<AuthorModel> Authors { get; set; }
        public PrintingEditionType PrintingEditionType { get; set; }

        public CatalogModelItem()
        {
            Authors = new List<AuthorModel>();
        }
    }
}
