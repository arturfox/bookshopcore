﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Catalog.Responses
{
    public class AuthorModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
