﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Checkout.Responses
{
    public class CheckoutSessionModel
    {
        public string SessionId { get; set; }
    }
}
