﻿using System.ComponentModel.DataAnnotations;

namespace BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Requests
{
    public class UpdateUserModel
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string ImageSource { get; set; }

        [RegularExpression("^$|^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z]).{6,}$", ErrorMessage = "Password must be at least 6 characters length, must contains one number, one capital letter and one lower case letter")]
        public string Password { get; set; }

        [Compare(nameof(Password), ErrorMessage = "Confirm password doesn't match!")]
        public string ConfirmPassword { get; set; }
    }
}
