﻿using BookStoreEducation.Entities.Enums;

namespace BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Responses
{
    public class UsersModelItem
    {
        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public AccountStatus AccountStatus { get; set; }
    }
}
