﻿using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Responses
{
    public class UsersModel
    {
        public IEnumerable<UsersModelItem> Users { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalItems { get; set; }

        public UsersModel()
        {
            Users = new List<UsersModelItem>();
        }
    }
}
