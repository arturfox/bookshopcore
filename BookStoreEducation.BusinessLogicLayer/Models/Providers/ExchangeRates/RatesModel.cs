﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.Providers.ExchangeRates
{
    public class RatesModel
    {
        [JsonProperty("rates")]
        public Dictionary<string, double> Rates { get; set; }
        public RatesModel()
        {
            Rates = new Dictionary<string, double>();
        }
    }
}
