﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Providers.S3
{
    public class UploadedFileModel
    {
        public string Key { get; set; }
        public string Region { get; set; }
        public string BucketName { get; set; }
    }
}
