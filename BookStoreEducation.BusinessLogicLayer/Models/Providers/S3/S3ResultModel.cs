﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Providers.S3
{
    public class S3ResultModel
    {
        public bool IsSucceed { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class S3ResultModel<TResult> : S3ResultModel
    {
        public TResult Data { get; set; }
    }
}
