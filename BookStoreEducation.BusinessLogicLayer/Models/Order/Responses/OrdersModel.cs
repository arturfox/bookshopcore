﻿using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.Order.Responses
{
    public class OrdersModel
    {
        public IEnumerable<OrdersModelItem> Orders { get; set; }
        public OrdersModel()
        {
            Orders = new List<OrdersModelItem>();
        }
    }
}
