﻿using BookStoreEducation.Entities.Enums;
using System;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.Order.Responses
{
    public class OrdersModelItem
    {
        public long OrderId { get; set; }
        public DateTime OrderTime { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public Currency Currency { get; set; }
        public double? Total { get; set; }
        public IEnumerable<OrderPositionModel> Positions { get; set; }

        public OrdersModelItem()
        {
            Positions = new List<OrderPositionModel>();
        }
    }
}