﻿using BookStoreEducation.Entities.Enums;
using System.Text.Json.Serialization;

namespace BookStoreEducation.BusinessLogicLayer.Models.Order.Responses
{
    public class OrderPositionModel
    {
        public PrintingEditionType PrintingEditionType { get; set; }
        public string Title { get; set; }
        public int Quantity { get; set; }

        [JsonIgnore]
        public double Price { get; set; }
        [JsonIgnore]
        public Currency Currency { get; set; }
    }
}
