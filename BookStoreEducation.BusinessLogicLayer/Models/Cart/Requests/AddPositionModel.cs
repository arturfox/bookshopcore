﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Cart.Requests
{
    public class AddPositionModel
    {
        public long PositionId { get; set; }
        public int Quantity { get; set; }
    }
}
