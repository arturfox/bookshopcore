﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Cart.Requests
{
    public class RemovePositionModel
    {
        public long CartItemId {get;set;}
    }
}
