﻿using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.Cart.Responses
{
    public class CartModel
    {
        public IEnumerable<CartModelItem> Items { get; set; }
        public Currency Currency { get; set; }
        public double Total { get; set; }

        public CartModel()
        {
            Items = new List<CartModelItem>();
        }
    }
}
