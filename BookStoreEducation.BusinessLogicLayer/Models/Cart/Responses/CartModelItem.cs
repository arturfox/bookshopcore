﻿using BookStoreEducation.Entities.Enums;

namespace BookStoreEducation.BusinessLogicLayer.Models.Cart.Responses
{
    public class CartModelItem
    {
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double Total { get; set; }
        public Currency Currency { get; set; }
        public string Title { get; set; }
        public string ImageSource { get; set; }
        public long PrintingEditionId { get; set; }
        public long CartItemId { get; set; }
    }
}
