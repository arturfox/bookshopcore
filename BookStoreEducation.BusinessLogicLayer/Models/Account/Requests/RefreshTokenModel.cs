﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Account.Requests
{
    public class RefreshTokenModel
    {
        public string RefreshToken { get; set; }
    }
}
