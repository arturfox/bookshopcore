﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Account.Requests
{
    public class RecoverPasswordModel
    {
        public string Email { get; set; }
    }
}
