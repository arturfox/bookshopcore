﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Account.Requests
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
