﻿using System.ComponentModel.DataAnnotations;

namespace BookStoreEducation.BusinessLogicLayer.Models.Account.Requests
{
    public class ChangePasswordModel
    {
        public string Password { get; set; }

        [Compare(nameof(Password), ErrorMessage = "Confirm password doesn't match!")]
        public string ConfirmPassword { get; set; }
    }
}
