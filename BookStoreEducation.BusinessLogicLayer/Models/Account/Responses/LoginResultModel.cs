﻿namespace BookStoreEducation.BusinessLogicLayer.Models.Account.Responses
{
    public class LoginResultModel
    {
        public string Token { get; set; }
        public string RefreshToken { get; set; }
    }
}
