﻿
namespace BookStoreEducation.BusinessLogicLayer.Models.Account.Responses
{
    public class ProfileModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string ImageSource { get; set; }
    }
}
