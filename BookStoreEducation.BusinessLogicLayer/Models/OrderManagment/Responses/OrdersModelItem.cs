﻿using BookStoreEducation.Entities.Enums;
using System;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.OrderManagment.Responses
{
    public class OrdersModelItem
    {
        public long OrderId { get; set; }
        public DateTime OrderTime { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public double? OrderAmount { get; set; }
        public Currency Currency { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public IEnumerable<OrderPositionModel> Positions { get; set; }

        public OrdersModelItem()
        {
            Positions = new List<OrderPositionModel>();
        }
    }
}
