﻿using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Models.OrderManagment.Responses
{
    public class OrdersModel
    {
        public IEnumerable<OrdersModelItem> Orders { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }
        public int TotalItems { get; set; }

        public OrdersModel()
        {
            Orders = new List<OrdersModelItem>();
        }
    }
}
