﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Cart.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.Cart.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services
{
    public class CartService : ICartService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ICartRepository _cartRepository;
        private readonly ICartPrintingEditionRepository _cartPrintingEditionRepository;
        private readonly IMapper _mapper;

        public CartService(ICartRepository cartRepository,
            UserManager<ApplicationUser> userManager,
            IMapper mapper, 
            ICartPrintingEditionRepository cartPrintingEditionRepository)
        {
            _cartRepository = cartRepository;
            _userManager = userManager;
            _mapper = mapper;
            _cartPrintingEditionRepository = cartPrintingEditionRepository;
        }

        public async Task AddCart(long userId)
        {
            var cart = new Cart()
            {
                UserId = userId
            };

            await _cartRepository.AddAsync(cart);
        }

        public async Task<ResponseModel> AddPosition(AddPositionModel positionModel, long userId)
        {
            var result = new ResponseModel();
            var user = await _userManager.Users.FirstOrDefaultAsync(user => user.Id == userId);
            if (user == null)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            var cart = await _cartRepository.GetCartByUserId(userId);
            var existingPosition = cart.CartPrintingEditions.FirstOrDefault(cpe => cpe.PrintingEditionId == positionModel.PositionId);
            if (existingPosition == null)
            {
                existingPosition = _mapper.Map<CartPrintingEdition>(positionModel);
                existingPosition.CartId = cart.Id;
                await _cartPrintingEditionRepository.AddAsync(existingPosition);
            }

            existingPosition.Quantity = positionModel.Quantity;
            await _cartPrintingEditionRepository.UpdateAsync(existingPosition);

            result.IsSucceed = true;
            return result;
        }

        public async Task<ResponseModel> RemovePosition(RemovePositionModel model, long userId)
        {
            var result = new ResponseModel();
            var user = await _userManager.Users.FirstOrDefaultAsync(user => user.Id == userId);
            if (user == null)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            var cart = await _cartRepository.GetCartByUserId(userId);

            var position = cart.CartPrintingEditions.FirstOrDefault(cpe => cpe.Id == model.CartItemId);
            if (position == null)
            {
                result.IsSucceed = false;
                result.Errors.Add("item not found");
                return result;
            }

            //todo: replace with change IsRemoved flag
            await _cartPrintingEditionRepository.RemoveAsync(position);

            result.IsSucceed = true;
            return result;
        }


        //todo: add converter functionality
        public async Task<ResponseModel<CartModel>> GetCart(long userId)
        {
            var result = new ResponseModel<CartModel>();
            var user = await _userManager.Users.FirstOrDefaultAsync(user => user.Id == userId);
            if (user == null)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            var cart = await _cartRepository.GetCartByUserId(userId);

            result.Data = new CartModel();
            result.Data.Items = _mapper.Map<List<CartModelItem>>(cart.PrintingEditions);
            result.Data.Total = 0;
            foreach (CartModelItem item in result.Data.Items)
            {
                var cartPrintingEdition = cart.CartPrintingEditions.FirstOrDefault(cpe => cpe.PrintingEditionId == item.PrintingEditionId);
                item.Quantity = cartPrintingEdition.Quantity;
                item.Total = item.Price * item.Quantity;
                item.CartItemId = cartPrintingEdition.Id;
                result.Data.Total += item.Total;
            }

            result.IsSucceed = true;
            return result;
        }
    }
}
