﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Catalog.Responses;
using BookStoreEducation.Entities.Enums;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services.Interfaces
{
    public interface ICatalogService
    {
        Task<ResponseModel<CatalogModel>> GetCatalog(string searchQuery, string categories, int? minValue, int? maxValue, 
            int takeItems, int page, Enums.Enums.SortType sortType, Currency currency);
        Task<ResponseModel<CatalogDetailsModel>> GetCatalogDetails(long printingEditionId);
    }
}
