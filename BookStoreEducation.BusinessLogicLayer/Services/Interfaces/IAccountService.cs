﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Account.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.Account.Responses;
using BookStoreEducation.Entities.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services.Interfaces
{
    public interface IAccountService
    {
        Task<ResponseModel> CreateAccount(CreateAccountModel accountModel);
        Task<IdentityResult> ConfirmEmail(ApplicationUser user, string token);
        Task<ResponseModel<LoginResultModel>> Login(LoginModel loginModel);
        Task<LoginResultModel> Login(ApplicationUser user);
        Task<ResponseModel<LoginResultModel>> RefreshToken(RefreshTokenModel refreshToken);
        Task<ResponseModel> RecoverPassword(RecoverPasswordModel recoverModel);
        Task<ResponseModel> ChangePassword(ChangePasswordModel model, long userId);
        Task<ResponseModel<ProfileModel>> GetProfile(long userId);
        Task<ResponseModel> UpdateProfile(UpdateProfileModel profileModel, long userId);
    }
}
