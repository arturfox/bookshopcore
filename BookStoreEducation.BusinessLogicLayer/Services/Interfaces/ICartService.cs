﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Cart.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.Cart.Responses;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services.Interfaces
{
    public interface ICartService
    {
        Task<ResponseModel<CartModel>> GetCart(long userId);
        Task<ResponseModel> AddPosition(AddPositionModel positionModel, long userId);
        Task<ResponseModel> RemovePosition(RemovePositionModel model, long userId);
        Task AddCart(long userId);
    }
}
