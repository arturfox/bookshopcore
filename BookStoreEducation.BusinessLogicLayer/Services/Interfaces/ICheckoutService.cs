﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Checkout.Responses;
using Stripe.Checkout;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services.Interfaces
{
    public interface ICheckoutService
    {
        Task<ResponseModel<CheckoutSessionModel>> CreateCheckoutSession(long userId);
        Task<ResponseModel<CheckoutSessionModel>> CreateCheckoutSession(long userId, long orderId);
        Task FulfillOrder(Session session);
    }
}
