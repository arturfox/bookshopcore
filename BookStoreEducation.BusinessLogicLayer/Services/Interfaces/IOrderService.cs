﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Order.Responses;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services.Interfaces
{
    public interface IOrderService
    {
        public Task<ResponseModel<OrdersModel>> GetOrders(long userId);
    }
}
