﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Enums;
using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Responses;
using BookStoreEducation.BusinessLogicLayer.Providers.Interfaces;
using BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.BusinessLogicLayer.Services.Admin
{
    public class UserManagmentService : IUserManagmentService
    {
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IMapper _mapper;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAWSS3Provider _aWSS3Provider;

        public UserManagmentService(IApplicationUserRepository applicationUserRepository,
            IMapper mapper,
            UserManager<ApplicationUser> userManager, 
            IAWSS3Provider aWSS3Provider)
        {
            _applicationUserRepository = applicationUserRepository;
            _mapper = mapper;
            _userManager = userManager;
            _aWSS3Provider = aWSS3Provider;
        }

        public async Task<ResponseModel<UsersModel>> GetUsers(string searchQuery, string acсountStates, UserManagmentSortType sortBy, int takeItems, int page)
        {
            var result = new ResponseModel<UsersModel>();
            result.Data = new UsersModel();

            int offset = page * takeItems;
            IEnumerable<AccountStatus> statuses = acсountStates.GetEnumsFromQuery<AccountStatus>();

            IEnumerable<string> keywords = searchQuery.GetKeywordsFromQuery();

            var users = await _applicationUserRepository.GetUsers(takeItems, offset, sortBy, statuses, keywords);

            result.Data = _mapper.Map<UsersModel>(users);
            result.Data.ItemsPerPage = takeItems;
            result.Data.CurrentPage = page + 1;

            result.IsSucceed = true;
            return result;
        }

        public async Task<ResponseModel> RemoveUser(long userId)
        {
            var result = new ResponseModel();

            var user = await _applicationUserRepository.GetAsync(userId);
            if (user == null || user.IsRemoved)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            var isAdmin = await _userManager.IsInRoleAsync(user, "Admin");
            if (isAdmin)
            {
                result.IsSucceed = false;
                result.Errors.Add("Admin's account cannot be removed or suspended");
                return result;
            }

            user.IsRemoved = true;
            await _applicationUserRepository.UpdateAsync(user);

            result.IsSucceed = true;
            return result;
        }

        public async Task<ResponseModel> UpdateUser(UpdateUserModel userModel)
        {
            var result = new ResponseModel();
            var user = await _applicationUserRepository.GetAsync(userModel.Id);
            if (user == null || user.IsRemoved)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            var isEmailUpdated = false;
            //update email
            if (!string.IsNullOrWhiteSpace(userModel.Email) && user.NormalizedEmail != userModel.Email.ToUpper())
            {
                var existingUser = await _userManager.FindByEmailAsync(userModel.Email);
                if (existingUser != null)
                {
                    result.IsSucceed = false;
                    result.Errors.Add("User with the same email already exists");
                    return result;
                }

                var updateEmailResult = await _userManager.SetEmailAsync(user, userModel.Email);
                if (!updateEmailResult.Succeeded)
                {
                    result.IsSucceed = false;
                    result.Errors.AddRange(updateEmailResult.Errors
                        .Select((error) => error.Description));

                    return result;
                }
                isEmailUpdated = true;
            }

            //update username
            if (!string.IsNullOrWhiteSpace(userModel.UserName) && user.NormalizedUserName != userModel.UserName.ToUpper())
            {
                var existingUser = await _userManager.FindByNameAsync(userModel.UserName);
                if (existingUser != null)
                {
                    result.IsSucceed = false;
                    result.Errors.Add("User with the same username already exists");
                    return result;
                }

                var updateUserNameResult = await _userManager.SetUserNameAsync(user, userModel.UserName);
                if (!updateUserNameResult.Succeeded)
                {
                    result.IsSucceed = false;
                    result.Errors.AddRange(updateUserNameResult.Errors
                        .Select((error) => error.Description));

                    return result;
                }
            }

            if (!string.IsNullOrWhiteSpace(userModel.Password))
            {
                var removePasswordResult = await _userManager.RemovePasswordAsync(user);
                if (!removePasswordResult.Succeeded)
                {
                    result.IsSucceed = false;
                    result.Errors.AddRange(removePasswordResult.Errors
                        .Select((error) => error.Description));

                    return result;
                }
                var setPasswordResult = await _userManager.AddPasswordAsync(user, userModel.Password);
                if (!setPasswordResult.Succeeded)
                {
                    result.IsSucceed = false;
                    result.Errors.AddRange(setPasswordResult.Errors
                        .Select((error) => error.Description));

                    return result;
                }
            }

            if (!string.IsNullOrWhiteSpace(userModel.FirstName))
            {
                user.FirstName = userModel.FirstName;
                user.NormalizedFirstName = userModel.FirstName.ToUpper();
            }

            if (!string.IsNullOrWhiteSpace(userModel.LastName))
            {
                user.LastName = userModel.LastName;
                user.NormalizedLastName = userModel.LastName.ToUpper();
            }
            //update imageSource
            if (!string.IsNullOrWhiteSpace(userModel.ImageSource))
            {
                if (user.ImageSource != null)
                {
                    user.ImageSource.IsRemoved = true;
                    await _aWSS3Provider.RemoveImage(user.ImageSource.BucketName, user.ImageSource.Key);
                }

                var uploadResult = await _aWSS3Provider.UploadImage(userModel.ImageSource, BucketAccessoryFileType.Profile);
                if (!uploadResult.IsSucceed)
                {
                    result.IsSucceed = false;
                    result.Errors.Add(uploadResult.ErrorMessage);
                    return result;
                }

                user.ImageSource = new ImageSource()
                {
                    BucketName = uploadResult.Data.BucketName,
                    Key = uploadResult.Data.Key,
                    Region = uploadResult.Data.Region
                };
            }

            if (isEmailUpdated)
            {
                user.EmailConfirmed = true;
            }

            await _applicationUserRepository.UpdateAsync(user);
            result.IsSucceed = true;

            return result;
        }

        public async Task<ResponseModel> UpdateUserAccountStatus(long userId, AccountStatus accountStatus)
        {
            var result = new ResponseModel();

            var user = await _applicationUserRepository.GetAsync(userId);
            if (user == null || user.IsRemoved)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            var isAdmin = await _userManager.IsInRoleAsync(user, "Admin");
            if (isAdmin)
            {
                result.IsSucceed = false;
                result.Errors.Add("Admin's account cannot be removed or suspended");
                return result;
            }

            user.AccountStatus = accountStatus;
            await _applicationUserRepository.UpdateAsync(user);

            result.IsSucceed = true;
            return result;
        }
    }
}
