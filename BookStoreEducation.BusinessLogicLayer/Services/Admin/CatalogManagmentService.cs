﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Responses;
using BookStoreEducation.BusinessLogicLayer.Providers.Interfaces;
using BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.BusinessLogicLayer.Services.Admin
{
    public class CatalogManagmentService : ICatalogManagmentService
    {
        private readonly IPrintingEditionRepository _printingEditionRepository;
        private readonly IAuthorRepository _authorRepository;
        private readonly IMapper _mapper;
        private readonly IAWSS3Provider _aWSS3Provider;

        public CatalogManagmentService(IPrintingEditionRepository printingEditionRepository,
            IAuthorRepository authorRepository,
            IMapper mapper,
            IAWSS3Provider aWSS3Provider)
        {
            _printingEditionRepository = printingEditionRepository;
            _authorRepository = authorRepository;
            _mapper = mapper;
            _aWSS3Provider = aWSS3Provider;
        }

        public async Task<ResponseModel<CatalogModel>> GetCatalog(string productCategories, CatalogManagmentSortType sortType, int takeItems, int page)
        {
            var result = new ResponseModel<CatalogModel>();
            result.Data = new CatalogModel();

            int offset = page * takeItems;
            IEnumerable<PrintingEditionType> criteria = productCategories.GetEnumsFromQuery<PrintingEditionType>();

            var printingEditions = await _printingEditionRepository
                .GetPrintingEditions(takeItems, offset, sortType, criteria);

            result.Data = _mapper.Map<CatalogModel>(printingEditions);
            result.Data.ItemsPerPage = takeItems;
            result.Data.CurrentPage = page + 1;

            result.IsSucceed = true;
            return result;
        }


        public async Task<ResponseModel> AddCatalogItem(AddCatalogItemModel catalogModelItem)
        {
            var printingEdition = _mapper.Map<PrintingEdition>(catalogModelItem);
            printingEdition.NormalizedTitle = printingEdition.Title.ToUpper();

            IEnumerable<Author> authors = await _authorRepository.GetRangeAsync(catalogModelItem.AuthorsIds);
            printingEdition.Authors = new List<Author>(authors);

            var result = new ResponseModel();

            if (!string.IsNullOrWhiteSpace(catalogModelItem.ImageSource))
            {
                var uploadResult = await _aWSS3Provider.UploadImage(catalogModelItem.ImageSource, Enums.BucketAccessoryFileType.Catalog);
                if (!uploadResult.IsSucceed)
                {
                    result.IsSucceed = false;
                    result.Errors.Add(uploadResult.ErrorMessage);
                    return result;
                }

                printingEdition.ImageSource = new ImageSource()
                {
                    BucketName = uploadResult.Data.BucketName,
                    Key = uploadResult.Data.Key,
                    Region = uploadResult.Data.Region
                };
            }


            await _printingEditionRepository.AddAsync(printingEdition);
            
            result.IsSucceed = true;

            return result;
        }

        public async Task<ResponseModel> RemoveItem(long Id)
        {
            var printingEdition = await _printingEditionRepository.GetPrintingEditionByIdDetailed(Id);

            var result = new ResponseModel();
            if (printingEdition == null || printingEdition.IsRemoved)
            {
                result.IsSucceed = false;
                result.Errors.Add("item not found");
                return result;
            }

            if (printingEdition.ImageSource != null)
            {
                printingEdition.ImageSource.IsRemoved = true;
                await _aWSS3Provider.RemoveImage(printingEdition.ImageSource.BucketName, printingEdition.ImageSource.Key);
            }

            printingEdition.IsRemoved = true;
            printingEdition.CartPrintingEditions.Clear();
            printingEdition.Authors.Clear();
            await _printingEditionRepository.UpdateAsync(printingEdition);

            result.IsSucceed = true;
            return result;
        }

        public async Task<ResponseModel> UpdateCatalogItem(UpdateCatalogItemModel catalogModelItem)
        {
            var printingEdition = await _printingEditionRepository.GetAsync(catalogModelItem.Id);

            var result = new ResponseModel();
            if (printingEdition == null || printingEdition.IsRemoved)
            {
                result.IsSucceed = false;
                result.Errors.Add("item not found");
                return result;
            }

            printingEdition = _mapper.Map<UpdateCatalogItemModel, PrintingEdition>(catalogModelItem, printingEdition);
            IEnumerable<Author> authors = await _authorRepository.GetRangeAsync(catalogModelItem.AuthorsIds);
            printingEdition.Authors = new List<Author>(authors);
            printingEdition.NormalizedTitle = printingEdition.Title.ToUpper();

            if (!string.IsNullOrWhiteSpace(catalogModelItem.ImageSource))
            {
                if (printingEdition.ImageSource != null)
                {
                    printingEdition.ImageSource.IsRemoved = true;
                    await _aWSS3Provider.RemoveImage(printingEdition.ImageSource.BucketName, printingEdition.ImageSource.Key);
                }

                var uploadResult = await _aWSS3Provider.UploadImage(catalogModelItem.ImageSource, Enums.BucketAccessoryFileType.Catalog);
                if (!uploadResult.IsSucceed)
                {
                    result.IsSucceed = false;
                    result.Errors.Add(uploadResult.ErrorMessage);
                    return result;
                }

                printingEdition.ImageSource = new ImageSource()
                {
                    BucketName = uploadResult.Data.BucketName,
                    Key = uploadResult.Data.Key,
                    Region = uploadResult.Data.Region
                };
            }

            await _printingEditionRepository.UpdateAsync(printingEdition);

            result.IsSucceed = true;
            return result;
        }
    }
}
