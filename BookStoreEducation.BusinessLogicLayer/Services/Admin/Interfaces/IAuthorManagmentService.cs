﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Responses;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces
{
    public interface IAuthorManagmentService
    {
        Task<ResponseModel<AuthorsModel>> GetAuthors(AuthorManagmentSortType sortType, int takeItems, int page);
        Task<ResponseModel<AllAuthorsModel>> GetAllAuthors();
        Task<ResponseModel> AddAuthor(AddAuthorModel authorModel);
        Task<ResponseModel> UpdateAuthor(UpdateAuthorModel updateModel);
        Task<ResponseModel> RemoveAuthor(long authorId);
    }
}
