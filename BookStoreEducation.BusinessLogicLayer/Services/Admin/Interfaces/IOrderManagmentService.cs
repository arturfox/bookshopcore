﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.OrderManagment.Responses;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces
{
    public interface IOrderManagmentService
    {
        Task<ResponseModel<OrdersModel>> GetOrders(string orderStates, OrderManagmentSortType sortBy, int takeItems, int page);
    }
}
