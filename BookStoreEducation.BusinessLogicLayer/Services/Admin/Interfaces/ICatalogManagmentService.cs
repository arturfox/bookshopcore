﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Responses;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces
{
    public interface ICatalogManagmentService
    {
        Task<ResponseModel<CatalogModel>> GetCatalog(string productCategories, CatalogManagmentSortType sortType, int takeItems, int page);
        Task<ResponseModel> AddCatalogItem(AddCatalogItemModel catalogModelItem);
        Task<ResponseModel> UpdateCatalogItem(UpdateCatalogItemModel catalogModelItem);
        Task<ResponseModel> RemoveItem(long id);
    }
}
