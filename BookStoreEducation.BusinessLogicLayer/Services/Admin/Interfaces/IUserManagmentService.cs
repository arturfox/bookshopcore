﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Responses;
using BookStoreEducation.Entities.Enums;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces
{
    public interface IUserManagmentService
    {
        Task<ResponseModel<UsersModel>> GetUsers(string searchQuery, string acсountStates, UserManagmentSortType sortBy, int takeItems, int page);
        Task<ResponseModel> RemoveUser(long userId);
        Task<ResponseModel> UpdateUserAccountStatus(long userId, AccountStatus accountStatus);
        Task<ResponseModel> UpdateUser(UpdateUserModel userModel);
    }
}
