﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.BusinessLogicLayer.Services.Admin
{
    public class AuthorManagmentService : IAuthorManagmentService
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IMapper _mapper;

        public AuthorManagmentService(IAuthorRepository authorRepository,
            IMapper mapper)
        {
            _authorRepository = authorRepository;
            _mapper = mapper;
        }

        public async Task<ResponseModel<AuthorsModel>> GetAuthors(AuthorManagmentSortType sortType, int takeItems, int page)
        {
            var result = new ResponseModel<AuthorsModel>();

            int offset = page * takeItems;

            var authors = await _authorRepository.GetAuthors(takeItems, offset, sortType);

            result.Data = _mapper.Map<AuthorsModel>(authors);
            result.Data.ItemsPerPage = takeItems;
            result.Data.CurrentPage = page + 1;

            result.IsSucceed = true;

            return result;
        }

        public async Task<ResponseModel<AllAuthorsModel>> GetAllAuthors()
        {
            var result = new ResponseModel<AllAuthorsModel>();
            
            var authors = await _authorRepository.GetAll();

            result.Data = _mapper.Map<AllAuthorsModel>(authors);
            result.IsSucceed = true;

            return result;
        }

        //todo: add check is author with the same name exists
        public async Task<ResponseModel> AddAuthor(AddAuthorModel authorModel)
        {
            var author = _mapper.Map<Author>(authorModel);
            author.NormalizedFirstName = author.FirstName.ToUpper();
            author.NormalizedLastName = author.LastName.ToUpper();

            await _authorRepository.AddAsync(author);

            var result = new ResponseModel();
            result.IsSucceed = true;

            return result;
        }

        public async Task<ResponseModel> RemoveAuthor(long authorId)
        {
            var result = new ResponseModel();

            var author = await _authorRepository.GetAsync(authorId);
            if (author == null || author.IsRemoved)
            {
                result.IsSucceed = false;
                result.Errors.Add("Author not found");
            }

            author.IsRemoved = true;
            await _authorRepository.UpdateAsync(author);

            result.IsSucceed = true;
            return result;
        }

        public async Task<ResponseModel> UpdateAuthor(UpdateAuthorModel updateModel)
        {
            var author = await _authorRepository.GetAsync(updateModel.Id);

            var result = new ResponseModel();
            if (author == null || author.IsRemoved)
            {
                result.IsSucceed = false;
                result.Errors.Add("author not found");
                return result;
            }

            author = _mapper.Map<UpdateAuthorModel, Author>(updateModel, author);
            author.NormalizedFirstName = author.FirstName.ToUpper();
            author.NormalizedLastName = author.LastName.ToUpper();
            await _authorRepository.UpdateAsync(author);

            result.IsSucceed = true;
            return result;
        }
    }
}
