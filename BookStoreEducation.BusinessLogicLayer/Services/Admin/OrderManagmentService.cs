﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.OrderManagment.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services.Admin
{
    public class OrderManagmentService : IOrderManagmentService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IMapper _mapper;

        public OrderManagmentService(IOrderRepository orderRepository, 
            IMapper mapper)
        {
            _orderRepository = orderRepository;
            _mapper = mapper;
        }

        public async Task<ResponseModel<OrdersModel>> GetOrders(string orderStates, Shared.Enums.Enums.OrderManagmentSortType sortBy, int takeItems, int page)
        {
            var result = new ResponseModel<OrdersModel>();
            result.Data = new OrdersModel();

            int offset = page * takeItems;
            IEnumerable<OrderStatus> criteria = orderStates.GetEnumsFromQuery<OrderStatus>();

            var orders = await _orderRepository.GetOrders(takeItems, offset, sortBy, criteria);

            result.Data = new OrdersModel();
            result.Data.Orders = _mapper.Map<List<OrdersModelItem>>(orders.Orders);

            foreach (var order in result.Data.Orders)
            {
                if (order.OrderAmount.HasValue)
                {
                    continue;
                }

                order.Currency = Currency.USD;

                decimal total = 0;
                foreach (var orderPosition in order.Positions)
                {
                    total += Convert.ToDecimal(orderPosition.Quantity * orderPosition.Price);
                }

                order.OrderAmount = Convert.ToDouble(total);
            }

            if (sortBy == Shared.Enums.Enums.OrderManagmentSortType.AmountAsc)
            {
                result.Data.Orders = result.Data.Orders.OrderBy(order => order.OrderAmount);
            }

            if (sortBy == Shared.Enums.Enums.OrderManagmentSortType.AmountDesc)
            {
                result.Data.Orders = result.Data.Orders.OrderByDescending(order => order.OrderAmount);
            }

            result.Data.ItemsPerPage = takeItems;
            result.Data.CurrentPage = page + 1;

            result.IsSucceed = true;
            return result;
        }
    }
}
