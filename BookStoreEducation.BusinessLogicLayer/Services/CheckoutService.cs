﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Configurations;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Cart.Responses;
using BookStoreEducation.BusinessLogicLayer.Models.Checkout.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Stripe;
using Stripe.Checkout;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BookStoreEducation.BusinessLogicLayer.Common.Constants.Constants;
using Order = BookStoreEducation.Entities.Entities.Order;

namespace BookStoreEducation.BusinessLogicLayer.Services
{
    public class CheckoutService : ICheckoutService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ICartRepository _cartRepository;
        private readonly IOptions<StripeOptions> _stripeOptions;
        private readonly IOptions<AppSettings> _appSettings;
        private readonly IMapper _mapper;
        private readonly IOrderRepository _orderRepository;
        private readonly ICartPrintingEditionRepository _cartPrintingEditionRepository;
        private readonly IPaymentRepository _paymentRepository;

        public CheckoutService(UserManager<ApplicationUser> userManager,
                               IOptions<StripeOptions> stripeOptions,
                               ICartRepository cartRepository,
                               IMapper mapper,
                               IOptions<AppSettings> appSettings,
                               IOrderRepository orderRepository,
                               ICartPrintingEditionRepository cartPrintingEditionRepository,
                               IPaymentRepository paymentRepository)
        {
            _userManager = userManager;
            _stripeOptions = stripeOptions;
            _cartRepository = cartRepository;
            _mapper = mapper;
            _appSettings = appSettings;
            _orderRepository = orderRepository;
            _cartPrintingEditionRepository = cartPrintingEditionRepository;
            _paymentRepository = paymentRepository;
        }

        #region Create checkout session

        #region Create checkout session by cart

        //todo: add converter
        public async Task<ResponseModel<CheckoutSessionModel>> CreateCheckoutSession(long userId)
        {
            var result = new ResponseModel<CheckoutSessionModel>();
            var user = await _userManager.Users.FirstOrDefaultAsync(user => user.Id == userId);
            if (user == null)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            //get cart items
            var cart = await _cartRepository.GetCartByUserId(userId);
            var items = _mapper.Map<List<CartModelItem>>(cart.PrintingEditions);
            foreach (CartModelItem item in items)
            {
                var cartPrintingEdition = cart.CartPrintingEditions.FirstOrDefault(cpe => cpe.PrintingEditionId == item.PrintingEditionId);
                item.Quantity = cartPrintingEdition.Quantity;
                item.CartItemId = cartPrintingEdition.Id;
            }

            //create local order record
            var order = new Order()
            {
                OrderStatus = OrderStatus.Unpaid,
                UserId = user.Id,
                OrderPositions = new List<OrderPosition>(items.Select(item => new OrderPosition()
                {
                    Count = item.Quantity,
                    PrintingEditionId = item.PrintingEditionId
                }))
            };
            await _orderRepository.AddAsync(order);

            //generate session
            var options = GetSessionCreateOptions(user, order);
            options.LineItems = GetSessionLineItemOptions(items);

            Session session;
            try
            {
                var service = new SessionService();
                session = service.Create(options);
            }
            catch (StripeException stripeException)
            {
                //rollback changes - remove order
                await _orderRepository.RemoveAsync(order);
                throw stripeException;
            }

            //clear cart
            await _cartPrintingEditionRepository.RemoveRangeAsync(cart.CartPrintingEditions);

            result.IsSucceed = true;
            result.Data = new CheckoutSessionModel()
            {
                SessionId = session.Id
            };

            return result;
        }

        private List<SessionLineItemOptions> GetSessionLineItemOptions(IEnumerable<CartModelItem> cartItems)
        {
            var sessionItems = new List<SessionLineItemOptions>();

            foreach (var item in cartItems)
            {
                var optionItem = new SessionLineItemOptions();
                optionItem.PriceData = new SessionLineItemPriceDataOptions()
                {
                    UnitAmount = (long)(item.Price * Payments.DollarToCenteCoefficient),
                    Currency = item.Currency.ToString()
                };
                optionItem.PriceData.ProductData = new SessionLineItemPriceDataProductDataOptions()
                {
                    Name = item.Title,
                };
                if (item.ImageSource != null)
                {
                    optionItem.PriceData.ProductData.Images = new List<string>(new string[] { item.ImageSource });
                }
                optionItem.Quantity = item.Quantity;

                sessionItems.Add(optionItem);
            }

            return sessionItems;
        }

        #endregion Create checkout session by cart

        #region Create checkout session by orderId

        public async Task<ResponseModel<CheckoutSessionModel>> CreateCheckoutSession(long userId, long orderId)
        {
            var result = new ResponseModel<CheckoutSessionModel>();
            var user = await _userManager.Users.FirstOrDefaultAsync(user => user.Id == userId);
            if (user == null)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            //get order
            var order = await _orderRepository.GetOrderById(orderId);
            if (order == null || order.IsRemoved || order.UserId != userId)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }
            if (order.OrderStatus == OrderStatus.Paid)
            {
                result.IsSucceed = false;
                result.Errors.Add("Order is already paid");
                return result;
            }

            //generate session
            var options = GetSessionCreateOptions(user, order);
            options.LineItems = GetSessionLineItemOptions(order);

            var service = new SessionService();
            var session = service.Create(options);

            result.IsSucceed = true;
            result.Data = new CheckoutSessionModel()
            {
                SessionId = session.Id
            };

            return result;
        }

        private List<SessionLineItemOptions> GetSessionLineItemOptions(Order order)
        {
            var sessionItems = new List<SessionLineItemOptions>();

            foreach (var item in order.OrderPositions)
            {
                var optionItem = new SessionLineItemOptions();

                optionItem.PriceData = new SessionLineItemPriceDataOptions()
                {
                    UnitAmount = (long)(item.PrintingEdition.Price * Payments.DollarToCenteCoefficient),
                    Currency = item.PrintingEdition.Currency.ToString()
                };
                optionItem.PriceData.ProductData = new SessionLineItemPriceDataProductDataOptions()
                {
                    Name = item.PrintingEdition.Title
                };
                if (item.PrintingEdition.ImageSource != null)
                {
                    optionItem.PriceData.ProductData.Images = new List<string>(new string[] { item.PrintingEdition.ImageSource.ObjectUrl });
                }
                optionItem.Quantity = item.Count;

                sessionItems.Add(optionItem);
            }

            return sessionItems;
        }

        #endregion  Create checkout session by orderId

        private SessionCreateOptions GetSessionCreateOptions(ApplicationUser user, Order order)
        {
            var options = new SessionCreateOptions();
            options.PaymentMethodTypes = new List<string>(new string[]
            {
                    Payments.Stripe.CardPaymentMethodType
            });
            options.Mode = Payments.Stripe.SessionPaymentMode;
            options.SuccessUrl = $"{_appSettings.Value.FrontedUrl}{_stripeOptions.Value.SuccessFrontendRoute}/{order.Id}";
            options.CancelUrl = $"{_appSettings.Value.FrontedUrl}{_stripeOptions.Value.CancelFrontendRoute}/{order.Id}";

            options.Metadata = new Dictionary<string, string>();
            options.Metadata.Add(Payments.Stripe.SessionMetadataUserIdKey, user.Id.ToString());
            options.Metadata.Add(Payments.Stripe.SessionMetadataEmailKey, user.Email);
            options.Metadata.Add(Payments.Stripe.SessionMetadataOrderIdKey, order.Id.ToString());

            return options;
        }

        #endregion Create checkout session

        #region Fulfill Order

        public async Task FulfillOrder(Session session)
        {
            var isParseSucceeded = session.Metadata.TryGetValue(Payments.Stripe.SessionMetadataOrderIdKey, out string data);
            if (!isParseSucceeded)
            {
                throw new System.Exception($"Can't get orderId. transactionId = {session.PaymentIntentId}");
            }

            isParseSucceeded = long.TryParse(data, out long orderId);
            if (!isParseSucceeded)
            {
                throw new System.Exception($"Can't parse orderId. transactionId = {session.PaymentIntentId}");
            }

            isParseSucceeded = System.Enum.TryParse<Currency>(session.Currency, true, out Currency currency);
            if (!isParseSucceeded)
            {
                throw new System.Exception($"Can't parse currency. transactionId = {session.PaymentIntentId}");
            }

            var Payment = new Payment()
            {
                TransactionId = session.PaymentIntentId,
                IsSucceed = true,
                Currency = currency,
                Amount = (double) session.AmountTotal / Payments.DollarToCenteCoefficient
            };
            await _paymentRepository.AddAsync(Payment);

            var order = await _orderRepository.GetAsync(orderId);
            order.OrderStatus = OrderStatus.Paid;
            order.PaymentId = Payment.Id;

            await _orderRepository.UpdateAsync(order);
        }

        #endregion Fulfill Order
    }
}
