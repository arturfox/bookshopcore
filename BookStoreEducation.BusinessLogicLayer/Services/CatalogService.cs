﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Catalog.Responses;
using BookStoreEducation.BusinessLogicLayer.Providers.Interfaces;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly IPrintingEditionRepository _printingEditionRepository;
        private readonly IMapper _mapper;
        private readonly IExchangeRatesProvider _exchangeRatesProvider;

        public CatalogService(IPrintingEditionRepository printingEditionRepository,
                              IMapper mapper, 
                              IExchangeRatesProvider exchangeRatesProvider)
        {
            _printingEditionRepository = printingEditionRepository;
            _mapper = mapper;
            _exchangeRatesProvider = exchangeRatesProvider;
        }

        #region get catalog

        public async Task<ResponseModel<CatalogModel>> GetCatalog(string searchQuery, string categories, int? minValue, int? maxValue, 
            int takeItems, int page, Enums.Enums.SortType sortType, Currency currency)
        {
            var result = new ResponseModel<CatalogModel>();
            result.Data = new CatalogModel();

            bool isSortByDesc = (sortType == Enums.Enums.SortType.HighToLow);
            int offset = page * takeItems;
            IEnumerable<PrintingEditionType> criteria = categories.GetEnumsFromQuery<PrintingEditionType>();
            IEnumerable<string> keywords = searchQuery.GetKeywordsFromQuery();

            var printingEditions =
                await _printingEditionRepository.GetPrintingEditions(takeItems, offset, minValue, maxValue, isSortByDesc, criteria, keywords);

            result.Data = _mapper.Map<CatalogModel>(printingEditions);
            result.Data.ItemsPerPage = takeItems;
            result.Data.CurrentPage = page + 1;

            var ratesResult = await _exchangeRatesProvider.GetRates(currency);
            foreach(var printingEditionItem in result.Data.Items)
            {
                bool isRateParsed = ratesResult.Rates.TryGetValue(printingEditionItem.BaseCurrency.ToString(), out double rate);
                if (isRateParsed)
                {
                    printingEditionItem.Currency = currency;
                    printingEditionItem.Price = Math.Round(printingEditionItem.BasePrice * rate, 2);
                    continue;
                }

                //use ininial currency & price if failed to get currency data
                printingEditionItem.Currency = printingEditionItem.BaseCurrency;
                printingEditionItem.Price = printingEditionItem.BasePrice;
            }

            result.IsSucceed = true;
            return result;
        }

        #endregion get catalog

        public async Task<ResponseModel<CatalogDetailsModel>> GetCatalogDetails(long printingEditionId)
        {
            var response = new ResponseModel<CatalogDetailsModel>();

            var printingEdition = await  _printingEditionRepository.GetPrintingEditionById(printingEditionId);

            if(printingEdition == null || printingEdition.IsRemoved)
            {
                response.IsSucceed = false;
                response.Errors.Add("Printing edition could not be found");
                return response;
            }

            response.IsSucceed = true;
            response.Data = _mapper.Map<CatalogDetailsModel>(printingEdition);

            return response;
        }
    }
}
