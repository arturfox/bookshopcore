﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Auth.Interfaces;
using BookStoreEducation.BusinessLogicLayer.Enums;
using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Helpers.Interfaces;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Account.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.Account.Responses;
using BookStoreEducation.BusinessLogicLayer.Providers.Interfaces;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services
{
    public class AccountService : IAccountService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IRefreshTokenRepository _refreshTokenRepository;
        private readonly IEmailSender _emailSender;
        private readonly IJwtFactory _jwtFactory;
        private readonly IEncodeTokenHelper _encodeTokenHelper;
        private readonly IPasswordHelper _passwordHelper;
        private readonly IApplicationUserRepository _applicationUserRepository;
        private readonly IMapper _mapper;
        private readonly IAWSS3Provider _aWSS3Provider;

        public AccountService(UserManager<ApplicationUser> userManager,
                              IEmailSender emailSender,
                              IJwtFactory jwtFactory,
                              IRefreshTokenRepository refreshTokenRepository,
                              IEncodeTokenHelper encodeTokenHelper,
                              SignInManager<ApplicationUser> signInManager,
                              IPasswordHelper passwordHelper,
                              IApplicationUserRepository applicationUserRepository,
                              IMapper mapper,
                              IAWSS3Provider aWSS3Provider)
        {
            _userManager = userManager;
            _emailSender = emailSender;
            _jwtFactory = jwtFactory;
            _refreshTokenRepository = refreshTokenRepository;
            _encodeTokenHelper = encodeTokenHelper;
            _signInManager = signInManager;
            _passwordHelper = passwordHelper;
            _applicationUserRepository = applicationUserRepository;
            _mapper = mapper;
            _aWSS3Provider = aWSS3Provider;
        }

        #region Create Account

        public async Task<ResponseModel> CreateAccount(CreateAccountModel accountModel)
        {
            var response = new ResponseModel();

            var user = new ApplicationUser()
            {
                UserName = accountModel.UserName,
                Email = accountModel.Email,
                FirstName = accountModel.FirstName,
                LastName = accountModel.LastName
            };

            var creationResult = await _userManager.CreateAsync(user, accountModel.Password);
            if (!creationResult.Succeeded)
            {
                response.IsSucceed = false;
                response.Errors.AddRange(creationResult.Errors
                            .Select((error) => error.Description));
                return response;
            }

            await _userManager.AddToRoleAsync(user, UserRole.Client.ToString());

            var confirmationToken = await GetEmailConfirmationToken(user);
            await _emailSender.SendEmailConfirmationLink(accountModel.Email, confirmationToken);

            response.IsSucceed = true;

            return response;
        }

        #endregion

        #region Confirm email

        public async Task<IdentityResult> ConfirmEmail(ApplicationUser user, string token)
        {
            var decodedToken = _encodeTokenHelper.DecodeToken(token);
            var result = await _userManager.ConfirmEmailAsync(user, decodedToken);
            return result;
        }

        private async Task<string> GetEmailConfirmationToken(ApplicationUser user)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            return _encodeTokenHelper.EncodeToken(token);
        }

        #endregion Confirm email

        #region Refresh token

        public async Task<ResponseModel<LoginResultModel>> RefreshToken(RefreshTokenModel model)
        {
            var result = new ResponseModel<LoginResultModel>();

            var token = await _refreshTokenRepository.GetRefreshToken(model.RefreshToken);
            if (token == null || !token.IsActive())
            {
                result.IsSucceed = false;
                result.Errors.Add("Token is Invalid");
                return result;
            }

            var user = _userManager.Users.FirstOrDefault(user => user.Id == token.UserId);
            if (user == null)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            token.Revoked = DateTime.UtcNow;
            await _refreshTokenRepository.UpdateAsync(token);

            result.IsSucceed = true;
            result.Data = await Login(user);

            return result;
        }

        #endregion Refresh token

        #region Login

        public async Task<ResponseModel<LoginResultModel>> Login(LoginModel loginModel)
        {
            var response = new ResponseModel<LoginResultModel>();

            var user = await _userManager.FindByNameAsync(loginModel.UserName)
                ?? await _userManager.FindByEmailAsync(loginModel.UserName);
            if (user == null || user.IsRemoved)
            {
                response.IsSucceed = false;
                response.Errors.Add("User not found");
                return response;
            }
            if (user.AccountStatus == AccountStatus.Suspended)
            {
                response.IsSucceed = false;
                response.Errors.Add("Account is suspended");
                return response;
            }

            var signInResult = await _signInManager.PasswordSignInAsync(user, loginModel.Password, false, false);
            if (!signInResult.Succeeded)
            {
                response.IsSucceed = false;
                response.Errors.Add("Password is not correct");
                return response;
            }

            response.IsSucceed = true;
            response.Data = await GetLoginResultModel(user);

            return response;
        }

        public Task<LoginResultModel> Login(ApplicationUser user)
        {
            return GetLoginResultModel(user);
        }

        private async Task<LoginResultModel> GetLoginResultModel(ApplicationUser user)
        {
            var accessToken = await GetAccessToken(user);

            var refreshToken = new RefreshToken()
            {
                AccessTokenPrint = accessToken,
                UserId = user.Id,
                Token = GenerateRefreshTokenPrint(),
            };
            refreshToken.Expires = refreshToken.CreationDate.Add(Common.Constants.Constants.Authentication.RefrefTokenLifespan);
            await _refreshTokenRepository.AddAsync(refreshToken);

            var model = new LoginResultModel()
            {
                Token = accessToken,
                RefreshToken = refreshToken.Token
            };

            return model;
        }

        #region generate access token

        private async Task<string> GetAccessToken(ApplicationUser user)
        {
            System.Collections.Generic.IList<string> roles = await _userManager.GetRolesAsync(user);
            var role = roles.FirstOrDefault();
            var identity = _jwtFactory.GenerateClaimsIdentity(user, role);

            return await GenerateJwt(identity, user.UserName);
        }

        private async Task<string> GenerateJwt(ClaimsIdentity identity, string userName)
        {
            var token = await _jwtFactory.GenerateEncodedToken(userName, identity);
            return token;
        }

        #endregion generate access token

        #region generate refresh token

        private string GenerateRefreshTokenPrint()
        {
            return Guid.NewGuid().ToString();
        }

        #endregion generate refresh token

        #endregion Login

        #region Recover password

        public async Task<ResponseModel> RecoverPassword(RecoverPasswordModel recoverModel)
        {
            var result = new ResponseModel();

            var user = await _userManager.FindByEmailAsync(recoverModel.Email);

            if(user == null || user.IsRemoved)
            {
                result.IsSucceed = false;
                result.Errors.Add("We're sorry. We weren't able to identify you given the informtion provided");
                return result;
            }

            await _userManager.RemovePasswordAsync(user);

            var newPassword = _passwordHelper.GenerateRandomPassword();
            var addPasswordResult = await _userManager.AddPasswordAsync(user, newPassword);

            if (!addPasswordResult.Succeeded)
            {
                result.IsSucceed = false;
                foreach(var error in addPasswordResult.Errors)
                {
                    result.Errors.Add(error.ToString());
                }

                return result;
            }

            await _emailSender.SendPassword(recoverModel.Email, newPassword);

            result.IsSucceed = true;
            return result;
        }

        #endregion Recover password

        #region Change password

        public async Task<ResponseModel> ChangePassword(ChangePasswordModel model, long userId)
        {
            var response = new ResponseModel();

            var user = await _applicationUserRepository.GetAsync(userId);
            if (user == null || user.IsRemoved)
            {
                response.IsSucceed = false;
                response.Errors.Add("User not found");
                return response;
            }

            var removePasswordResult = await _userManager.RemovePasswordAsync(user);
            if (!removePasswordResult.Succeeded)
            {
                response.IsSucceed = false;
                response.Errors.AddRange(removePasswordResult.Errors
                    .Select((error) => error.Description));

                return response;
            }
            var setPasswordResult = await _userManager.AddPasswordAsync(user, model.Password);
            if (!setPasswordResult.Succeeded)
            {
                response.IsSucceed = false;
                response.Errors.AddRange(setPasswordResult.Errors
                    .Select((error) => error.Description));
            }

            response.IsSucceed = true;
            return response;
        }

        #endregion Change password

        #region Get profile

        public async Task<ResponseModel<ProfileModel>> GetProfile(long userId)
        {
            var response = new ResponseModel<ProfileModel>();

            var user = await _applicationUserRepository.GetUserById(userId);
            if (user == null || user.IsRemoved)
            {
                response.IsSucceed = false;
                response.Errors.Add("User not found");
                return response;
            }

            response.IsSucceed = true;
            response.Data = _mapper.Map<ProfileModel>(user);

            return response;
        }

        #endregion Get profile

        #region Update profile

        //todo: combine with  usermanagment -> UpdateUser
        public async Task<ResponseModel> UpdateProfile(UpdateProfileModel profileModel, long userId)
        {
            var result = new ResponseModel<ProfileModel>();

            var user = await _applicationUserRepository.GetUserById(userId);
            if (user == null || user.IsRemoved)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            var isEmailUpdated = false;
            //update email
            if (!string.IsNullOrWhiteSpace(profileModel.Email) && user.NormalizedEmail != profileModel.Email.ToUpper())
            {
                var existingUser = await _userManager.FindByEmailAsync(profileModel.Email);
                if (existingUser != null)
                {
                    result.IsSucceed = false;
                    result.Errors.Add("User with the same email already exists");
                    return result;
                }

                var updateEmailResult = await _userManager.SetEmailAsync(user, profileModel.Email);
                if (!updateEmailResult.Succeeded)
                {
                    result.IsSucceed = false;
                    result.Errors.AddRange(updateEmailResult.Errors
                        .Select((error) => error.Description));

                    return result;
                }
                isEmailUpdated = true;
            }

            //update username
            if (!string.IsNullOrWhiteSpace(profileModel.UserName) && user.NormalizedUserName != profileModel.UserName.ToUpper())
            {
                var existingUser = await _userManager.FindByNameAsync(profileModel.UserName);
                if (existingUser != null)
                {
                    result.IsSucceed = false;
                    result.Errors.Add("User with the same username already exists");
                    return result;
                }

                var updateUserNameResult = await _userManager.SetUserNameAsync(user, profileModel.UserName);
                if (!updateUserNameResult.Succeeded)
                {
                    result.IsSucceed = false;
                    result.Errors.AddRange(updateUserNameResult.Errors
                        .Select((error) => error.Description));

                    return result;
                }
            }
          
            if (!string.IsNullOrWhiteSpace(profileModel.FirstName))
            {
                user.FirstName = profileModel.FirstName;
                user.NormalizedFirstName = profileModel.FirstName.ToUpper();
            }
            if (!string.IsNullOrWhiteSpace(profileModel.LastName))
            {
                user.LastName = profileModel.LastName;
                user.NormalizedLastName = profileModel.LastName.ToUpper();
            }
            //update imageSource
            if (!string.IsNullOrWhiteSpace(profileModel.ImageSource))
            {
                if(user.ImageSource != null)
                {
                    user.ImageSource.IsRemoved = true;
                    await _aWSS3Provider.RemoveImage(user.ImageSource.BucketName, user.ImageSource.Key);
                }

                var uploadResult = await _aWSS3Provider.UploadImage(profileModel.ImageSource, BucketAccessoryFileType.Profile);
                if (!uploadResult.IsSucceed)
                {
                    result.IsSucceed = false;
                    result.Errors.Add(uploadResult.ErrorMessage);
                    return result;
                }

                user.ImageSource = new ImageSource()
                {
                    BucketName = uploadResult.Data.BucketName,
                    Key = uploadResult.Data.Key,
                    Region = uploadResult.Data.Region
                };
            }

            if (!string.IsNullOrWhiteSpace(profileModel.Password))
            {
                var removePasswordResult = await _userManager.RemovePasswordAsync(user);
                if (!removePasswordResult.Succeeded)
                {
                    result.IsSucceed = false;
                    result.Errors.AddRange(removePasswordResult.Errors
                        .Select((error) => error.Description));

                    return result;
                }
                var setPasswordResult = await _userManager.AddPasswordAsync(user, profileModel.Password);
                if (!setPasswordResult.Succeeded)
                {
                    result.IsSucceed = false;
                    result.Errors.AddRange(setPasswordResult.Errors
                        .Select((error) => error.Description));

                    return result;
                }
            }

            if (isEmailUpdated)
            {
                user.EmailConfirmed = true;
            }

            await _applicationUserRepository.UpdateAsync(user);

            result.IsSucceed = true;
            return result;
        }

        #endregion Update profile
    }
}
