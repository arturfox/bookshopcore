﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Order.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Services
{
    public class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IMapper _mapper;

        public OrderService(IOrderRepository orderRepository,
            UserManager<ApplicationUser> userManager, 
            IMapper mapper)
        {
            _orderRepository = orderRepository;
            _userManager = userManager;
            _mapper = mapper;
        }


        //todo: add price converter
        public async Task<ResponseModel<OrdersModel>> GetOrders(long userId)
        {
            var result = new ResponseModel<OrdersModel>();
            var user = await _userManager.Users.FirstOrDefaultAsync(user => user.Id == userId);
            if (user == null)
            {
                result.IsSucceed = false;
                result.Errors.Add("User not found");
                return result;
            }

            IEnumerable<Order> orders = await _orderRepository.GetOrdersByUserId(userId);

            result.Data = new OrdersModel();
            result.Data.Orders = _mapper.Map<List<OrdersModelItem>>(orders);

            
            foreach(var order in result.Data.Orders)
            {
                //value mapped from payment when payment exists
                if (order.Total.HasValue)
                {
                    continue;
                }

                order.Currency = Entities.Enums.Currency.USD;

                decimal total = 0;
                foreach (var orderPosition in order.Positions)
                {
                    total += Convert.ToDecimal(orderPosition.Quantity * orderPosition.Price);
                }

                order.Total = Convert.ToDouble(total);
            }

            result.IsSucceed = true;
            return result;
        }
    }
}
