﻿using Amazon.Runtime;
using Amazon.S3;
using Amazon.S3.Transfer;
using BookStoreEducation.BusinessLogicLayer.Configurations;
using BookStoreEducation.BusinessLogicLayer.Enums;
using BookStoreEducation.BusinessLogicLayer.Models.Providers.S3;
using BookStoreEducation.BusinessLogicLayer.Providers.Interfaces;
using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Providers
{
    public class AWSS3Provider : IAWSS3Provider
    {
        private readonly StorageServiceOptions _storageServiceOptions;

        public AWSS3Provider(IOptions<StorageServiceOptions> storageServiceOptions)
        {
            _storageServiceOptions = storageServiceOptions.Value;
        }

        public async Task<S3ResultModel> RemoveImage(string bucketName, string key)
        {
            var credentials = new BasicAWSCredentials(_storageServiceOptions.AccessKeyId, _storageServiceOptions.SecretAccessKey);
            var config = new AmazonS3Config
            {
                RegionEndpoint = Amazon.RegionEndpoint.GetBySystemName(_storageServiceOptions.Region)
            };
            using var client = new AmazonS3Client(credentials, config);

            var deleteResponse = await client.DeleteObjectAsync(bucketName, key);

            var result = new S3ResultModel();
            result.IsSucceed = (deleteResponse.HttpStatusCode == HttpStatusCode.OK);

            return result;
        }

        public async Task<S3ResultModel<UploadedFileModel>> UploadImage(string base64Data, BucketAccessoryFileType accessoryFileType)
        {
            var credentials = new BasicAWSCredentials(_storageServiceOptions.AccessKeyId, _storageServiceOptions.SecretAccessKey);
            var config = new AmazonS3Config
            {
                RegionEndpoint = Amazon.RegionEndpoint.GetBySystemName(_storageServiceOptions.Region)
            };
            using var client = new AmazonS3Client(credentials, config);

            //remove base64 header
            base64Data = base64Data.Substring(base64Data.IndexOf(',') + 1);

            var fileKey = GenerateFileKeyByBucketAccessoryFileType(accessoryFileType);
            await using (var memoryStream = new MemoryStream(Convert.FromBase64String(base64Data)))
            {
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = memoryStream,
                    Key = fileKey,
                    BucketName = _storageServiceOptions.BucketName,
                    CannedACL = S3CannedACL.PublicRead
                };

                var fileTransferUtility = new TransferUtility(client);
                await fileTransferUtility.UploadAsync(uploadRequest);
            }

            var result = new S3ResultModel<UploadedFileModel>();

            result.IsSucceed = true;
            result.Data = new UploadedFileModel()
            {
                BucketName = _storageServiceOptions.BucketName,
                Region = _storageServiceOptions.Region,
                Key = fileKey
            };

            return result;
        }

        private string GenerateFileKeyByBucketAccessoryFileType(BucketAccessoryFileType accessoryFileType)
        {
            return $"{Enum.GetName(typeof(BucketAccessoryFileType), accessoryFileType)}/{Guid.NewGuid()}";
        }
    }
}
