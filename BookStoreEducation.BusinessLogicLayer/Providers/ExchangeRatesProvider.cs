﻿using BookStoreEducation.BusinessLogicLayer.Configurations;
using BookStoreEducation.BusinessLogicLayer.Helpers.Interfaces;
using BookStoreEducation.BusinessLogicLayer.Models.Providers.ExchangeRates;
using BookStoreEducation.BusinessLogicLayer.Providers.Interfaces;
using BookStoreEducation.Entities.Enums;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Providers
{
    public class ExchangeRatesProvider : IExchangeRatesProvider
    {
        private readonly IHttpHelper _httpHelper;
        private readonly string _apiUrl;

        public ExchangeRatesProvider(IHttpHelper httpHelper,
            IOptions<CurrencyProviderOptions> providerOptions)
        {
            _httpHelper = httpHelper;
            _apiUrl = providerOptions.Value.Url;
        }

        public Task<RatesModel> GetRates(Currency baseCurrency)
        {
            return _httpHelper.Get<RatesModel>($"{_apiUrl}latest?base={baseCurrency}");
        }
    }
}
