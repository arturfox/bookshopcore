﻿using BookStoreEducation.BusinessLogicLayer.Configurations;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Providers.Interfaces;
using Microsoft.Extensions.Options;
using System.Net.Mail;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Providers
{
    public class EmailSender : IEmailSender
    {
        private readonly SmtpOptions _options;
        private readonly string _domain;
        public EmailSender(IOptions<SmtpOptions> options,
            IOptions<AppSettings> appSettings)
        {
            _options = options.Value;
            _domain = appSettings.Value.BackendUrl;
        }

        #region Send confirmation email
        public async Task SendEmailConfirmationLink(string email, string token)
        {
            var message = GetEmailConfirmationLinkMessage(email, token);
            FireSMTPSend(message.EmailTo, message.Subject, message.MailBody);
        }

        private EmailModel GetEmailConfirmationLinkMessage(string email, string token)
        {
            string link = $"{_domain}/Account/EmailConfirmation?token={token}&email={email}";
            string htmlBody = $"<a href=\"{link}\">Confirm Email</a>";

            var mailModel = new EmailModel
            {
                EmailTo = email,
                Subject = "Email confirmation",
                MailBody = htmlBody
            };
            return mailModel;
        }

        #endregion Send confirmation email

        #region Send recovered password
        public async Task SendPassword(string email, string password)
        {
            var message = GetRecoveredPasswordMessage(email, password);
            FireSMTPSend(message.EmailTo, message.Subject, message.MailBody);
        }

        private EmailModel GetRecoveredPasswordMessage(string email, string password)
        {
            string htmlBody = $"<span>Your new password: <h3>{password}</h3></span>";
          
            var mailModel = new EmailModel
            {
                EmailTo = email,
                Subject = "Password recovery",
                MailBody = htmlBody
            };
            return mailModel;
        }

        #endregion Send recovered password

        private void FireSMTPSend(string toEmail, string subject, string mailBody)
        {
            var mailMessage = BuildMessage(toEmail, subject, mailBody);
            using (var smtpClient = new SmtpClient(_options.Host))
            {
                smtpClient.Port = _options.Port;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential(_options.UserName, _options.Password);
                smtpClient.EnableSsl = _options.UseSsl;

                smtpClient.Send(mailMessage);
            }
        }

        private MailMessage BuildMessage(string toEmail, string subject, string body)
        {
            var message = new MailMessage();
            message.Body = body;
            message.From = new MailAddress(_options.Email, _options.DisplayName);
            message.To.Add(toEmail);
            message.Subject = subject;
            message.IsBodyHtml = true;

            return message;
        }
    }
}
