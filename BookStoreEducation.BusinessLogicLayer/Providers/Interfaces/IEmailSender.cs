﻿using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Providers.Interfaces
{
    public interface IEmailSender
    {
        Task SendEmailConfirmationLink(string email, string token);
        Task SendPassword(string email, string password);
    }
}
