﻿using BookStoreEducation.BusinessLogicLayer.Models.Providers.ExchangeRates;
using BookStoreEducation.Entities.Enums;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Providers.Interfaces
{
    public interface IExchangeRatesProvider
    {
        Task<RatesModel> GetRates(Currency baseCurrency);
    }
}
