﻿using BookStoreEducation.BusinessLogicLayer.Enums;
using BookStoreEducation.BusinessLogicLayer.Models.Providers.S3;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Providers.Interfaces
{
    public interface IAWSS3Provider
    {
        Task<S3ResultModel<UploadedFileModel>> UploadImage(string base64Data, BucketAccessoryFileType accessoryFileType);
        Task<S3ResultModel> RemoveImage(string bucketName, string key);
    }
}
