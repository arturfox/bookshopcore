﻿using BookStoreEducation.BusinessLogicLayer.Auth;
using System;
using System.Security.Claims;
using System.Security.Principal;

namespace BookStoreEducation.BusinessLogicLayer.Extensions
{
    public static class IdentityExtensions
    {
        public static long? GetUserId(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
            Claim claim = claimsIdentity?.FindFirst(Constants.JwtClaimIdentifiers.Id);
            if (claim == null)
            {
                return null;
            }
            return long.Parse(claim.Value);
        }

        public static string GetUserRole(this IIdentity identity)
        {
            ClaimsIdentity claimsIdentity = identity as ClaimsIdentity;
            Claim claim = claimsIdentity?.FindFirst(Constants.JwtClaimIdentifiers.Role);
            if (claim == null)
            {
                return String.Empty;
            }
            return claim.Value;
        }
    }
}
