﻿using System;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.Extensions
{
    public static class StringExtensions
    {
        public static IEnumerable<T> GetEnumsFromQuery<T>(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            var typesList = new List<T>();
            string[] array = value.Split(new char[] { ',' });

            foreach (var elem in array)
            {
                if (elem == "" || elem == ",")
                {
                    continue;
                }
                T type = elem.ToEnum<T>();
                typesList.Add(type);
            }

            return typesList;
        }
        public static IEnumerable<string> GetKeywordsFromQuery(this string searchQuery)
        {
            if (string.IsNullOrWhiteSpace(searchQuery))
            {
                return null;
            }

            var keywords = new List<string>();
            string[] array = searchQuery.Split(new char[] { '%' });

            foreach (var elem in array)
            {
                if (elem == string.Empty || elem == "%")
                {
                    continue;
                }

                keywords.Add(elem);
            }

            return keywords;
        }

        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
    }
}
