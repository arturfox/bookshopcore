﻿using BookStoreEducation.Entities.Entities;
using System;

namespace BookStoreEducation.BusinessLogicLayer.Extensions
{
    public static class RefreshTokenExtensions
    {
        public static bool IsActive(this RefreshToken token)
        {
            return token.Revoked == null && !token.IsExpired(); 
        }

        public static bool IsExpired(this RefreshToken token)
        {
            return DateTime.UtcNow >= token.Expires;
        }
    }
}
