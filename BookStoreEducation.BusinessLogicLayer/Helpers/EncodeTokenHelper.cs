﻿using BookStoreEducation.BusinessLogicLayer.Helpers.Interfaces;
using System;
using System.Text;

namespace BookStoreEducation.BusinessLogicLayer.Helpers
{
    public class EncodeTokenHelper : IEncodeTokenHelper
    {
        public string EncodeToken(string token)
        {
            byte[] tokenByte = Encoding.UTF8.GetBytes(token);
            string encodedToken = Convert.ToBase64String(tokenByte);
            return encodedToken;
        }
        public string DecodeToken(string token)
        {
            byte[] tokenByte = Convert.FromBase64String(token);
            string decodedToken = Encoding.UTF8.GetString(tokenByte);
            return decodedToken;
        }
    }
}
