﻿using BookStoreEducation.BusinessLogicLayer.Helpers.Interfaces;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Helpers
{
	public class HttpHelper : IHttpHelper
	{
		public async Task<T> Get<T>(string url)
		{
			using (var client = new HttpClient())
			{
				HttpResponseMessage response = await client.GetAsync(url);

				string responseString = await response.Content.ReadAsStringAsync();
				T view = JsonConvert.DeserializeObject<T>(responseString);

				return view;
			}
		}
	}
}
