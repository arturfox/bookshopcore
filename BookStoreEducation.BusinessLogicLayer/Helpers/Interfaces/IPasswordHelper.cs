﻿using Microsoft.AspNetCore.Identity;

namespace BookStoreEducation.BusinessLogicLayer.Helpers.Interfaces
{
    public interface IPasswordHelper
    {
        string GenerateRandomPassword(PasswordOptions opts = null);
    }
}
