﻿using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Helpers.Interfaces
{
	public interface IHttpHelper
	{
		Task<T> Get<T>(string url);
	}
}
