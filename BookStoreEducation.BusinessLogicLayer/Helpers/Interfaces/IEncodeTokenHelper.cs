﻿namespace BookStoreEducation.BusinessLogicLayer.Helpers.Interfaces
{
    public interface IEncodeTokenHelper
    {
        string EncodeToken(string token);
        string DecodeToken(string token);
    }
}
