﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Configurations;
using BookStoreEducation.BusinessLogicLayer.Helpers;
using BookStoreEducation.BusinessLogicLayer.Helpers.Interfaces;
using BookStoreEducation.BusinessLogicLayer.MappingProfiles;
using BookStoreEducation.BusinessLogicLayer.Providers;
using BookStoreEducation.BusinessLogicLayer.Providers.Interfaces;
using BookStoreEducation.BusinessLogicLayer.Services;
using BookStoreEducation.BusinessLogicLayer.Services.Admin;
using BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStoreEducation.BusinessLogicLayer
{
    public class DependencyManager
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            DataAccessLayer.DependencyManager.Configure(services, configuration);

            services.Configure<SmtpOptions>(configuration.GetSection("Smtp"));
            services.Configure<AppSettings>(configuration.GetSection("AppSettings"));
            services.Configure<StripeOptions>(configuration.GetSection("Stripe"));
            services.Configure<CurrencyProviderOptions>(configuration.GetSection("exchangeratesapi.io"));
            services.Configure<StorageServiceOptions>(configuration.GetSection("AWSS3Bucket"));
            
            services.AddTransient<IEncodeTokenHelper, EncodeTokenHelper>();
            services.AddTransient<IPasswordHelper, PasswordHelper>();
            services.AddTransient<IHttpHelper, HttpHelper>();

            services.AddTransient<IEmailSender, EmailSender>();
            services.AddTransient<IExchangeRatesProvider, ExchangeRatesProvider>();
            services.AddTransient<IAWSS3Provider, AWSS3Provider>();

            services.AddTransient<IAccountService, AccountService>();
            services.AddTransient<ICatalogService, CatalogService>();
            services.AddTransient<ICartService, CartService>();
            services.AddTransient<ICheckoutService, CheckoutService>();
            services.AddTransient<IOrderService, OrderService>();
            services.AddTransient<ICatalogManagmentService, CatalogManagmentService>();
            services.AddTransient<IAuthorManagmentService, AuthorManagmentService>();
            services.AddTransient<IUserManagmentService, UserManagmentService>();
            services.AddTransient<IOrderManagmentService, OrderManagmentService>();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new CatalogProfile());
                mc.AddProfile(new CartProfile());
                mc.AddProfile(new OrderProfile());
                mc.AddProfile(new CatalogManagmentProfile());
                mc.AddProfile(new AuthorManagmentProfile());
                mc.AddProfile(new UserManagmentProfile());
                mc.AddProfile(new OrderManagmentProfile());
                mc.AddProfile(new AccountProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }
    }
}
