﻿namespace BookStoreEducation.BusinessLogicLayer.Common.Constants
{
    public partial class Constants
    {
        public class Pagination
        {
            public class Catalog
            {
                public const int ItemsCountPerPage = 20;
            }
            public class CatalogManagment
            {
                public const int ItemsCountPerPage = 20;
            }
            public class AuthorManagment
            {
                public const int ItemsCountPerPage = 20;
            }
            public class UserManagment
            {
                public const int ItemsCountPerPage = 20;
            }
            public class OrderManagment
            {
                public const int ItemsCountPerPage = 20;
            }
        }      
    }
}
