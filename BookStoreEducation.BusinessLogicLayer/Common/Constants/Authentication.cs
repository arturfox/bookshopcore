﻿using System;

namespace BookStoreEducation.BusinessLogicLayer.Common.Constants
{
    public partial class Constants
    {
        public partial class Authentication
        {
            public static TimeSpan EmailConfirmationTokenLifespan => TimeSpan.FromDays(7);
            public static TimeSpan RefrefTokenLifespan => TimeSpan.FromDays(1);

            //todo: remove hardcode
            public static TimeSpan AccessTokenLifespan => /*TimeSpan.FromMinutes(5);*/ TimeSpan.FromDays(1);

            public partial class PasswordCrypto
            {
                public const string Uppercase = "ABCDEFGHJKLMNOPQRSTUVWXYZ";
                public const string Lowercase = "abcdefghijkmnopqrstuvwxyz";
                public const string Digits = "abcdefghijkmnopqrstuvwxyz";
                public const string NonAlphanumeric = "!@$?_-";
            }
        }
    }
}
