﻿namespace BookStoreEducation.BusinessLogicLayer.Common.Constants
{
    public partial class Constants
    {
        public partial class Payments
        {
            public const int DollarToCenteCoefficient = 100;

            public partial class Stripe
            {
                public const string SessionPaymentMode = "payment";
                public const string CardPaymentMethodType = "card";
                public const string USDCurrency = "usd";
                public const string SessionMetadataUserIdKey = "userId";
                public const string SessionMetadataEmailKey = "email";
                public const string SessionMetadataOrderIdKey = "orderId";
                public const string StripeSignatureHeader = "Stripe-Signature";
            }
        }
    }
}
