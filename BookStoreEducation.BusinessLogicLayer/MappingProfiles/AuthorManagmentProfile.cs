﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Responses;
using BookStoreEducation.DataAccessLayer.Models.Author;
using BookStoreEducation.Entities.Entities;
using System.Collections.Generic;

namespace BookStoreEducation.BusinessLogicLayer.MappingProfiles
{
    public class AuthorManagmentProfile : Profile
    {
        public AuthorManagmentProfile()
        {
            CreateMap<AddAuthorModel, Author>();

            CreateMap<UpdateAuthorModel, Author>()
                .ForMember(destin => destin.Id, opt => opt.Ignore());

            CreateMap<GetAuthorsModel, AuthorsModel>()
                .ForMember(destin => destin.Items, opt => opt.MapFrom(src => src.Authors));
            CreateMap<Author, AuthorsModelItem>()
                .ForMember(destin => destin.PrintingEditions, opt => opt.MapFrom(src => src.PrintingEditions));
            CreateMap<PrintingEdition, PrintingEditionModel>();

            CreateMap<List<Author>, AllAuthorsModel>()
                .ForMember(destin => destin.Items, opt => opt.MapFrom(src => src));
            CreateMap<Author, AllAuthorsModelItem>();      
        }
    }
}
