﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models.OrderManagment.Responses;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.BusinessLogicLayer.MappingProfiles
{
    public class OrderManagmentProfile : Profile
    {
        public OrderManagmentProfile()
        {
            CreateMap<OrderPosition, OrderPositionModel>()
                .ForMember(destin => destin.Quantity, opt => opt.MapFrom(src => src.Count))
                .ForMember(destin => destin.Title, opt => opt.MapFrom(src => src.PrintingEdition.Title))
                .ForMember(destin => destin.PrintingEditionType, opt => opt.MapFrom(src => src.PrintingEdition.PrintingEditionType))
                .ForMember(destin => destin.Currency, opt => opt.MapFrom(src => src.PrintingEdition.Currency))
                .ForMember(destin => destin.Price, opt => opt.MapFrom(src => src.PrintingEdition.Price));

            CreateMap<Order, OrdersModelItem>()
                 .ForMember(destin => destin.OrderId, opt => opt.MapFrom(src => src.Id))
                 .ForMember(destin => destin.OrderTime, opt => opt.MapFrom(src => src.CreationDate))
                 .ForMember(destin => destin.Positions, opt => opt.MapFrom(src => src.OrderPositions))
                 .ForMember(destin => destin.UserName, opt => opt.MapFrom(src => src.User.UserName))
                 .ForMember(destin => destin.Email, opt => opt.MapFrom(src => src.User.Email));
        }
    }
}
