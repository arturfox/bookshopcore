﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models.Order.Responses;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;

namespace BookStoreEducation.BusinessLogicLayer.MappingProfiles
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {

            CreateMap<OrderPosition, OrderPositionModel>()
                .ForMember(destin => destin.Quantity, opt => opt.MapFrom(src => src.Count))
                .ForMember(destin => destin.Title, opt => opt.MapFrom(src => src.PrintingEdition.Title))
                .ForMember(destin => destin.PrintingEditionType, opt => opt.MapFrom(src => src.PrintingEdition.PrintingEditionType))
                .ForMember(destin => destin.Currency, opt => opt.MapFrom(src => src.PrintingEdition.Currency))
                .ForMember(destin => destin.Price, opt => opt.MapFrom(src => src.PrintingEdition.Price));

            CreateMap<Order, OrdersModelItem>()
                 .ForMember(destin => destin.OrderId, opt => opt.MapFrom(src => src.Id))
                 .ForMember(destin => destin.OrderTime, opt => opt.MapFrom(src => src.CreationDate))
                 .ForMember(destin => destin.Positions, opt => opt.MapFrom(src => src.OrderPositions))
                 .ForMember(destin => destin.Currency, opt => opt.MapFrom(src => (src.Payment == null) ? Currency.None : src.Payment.Currency))
                 .ForMember(destin => destin.Total, opt => opt.MapFrom(src => (src.Payment == null) ? null : src.Payment.Amount));
        }
    }
}
