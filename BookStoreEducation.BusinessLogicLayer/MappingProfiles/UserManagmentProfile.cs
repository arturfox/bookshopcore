﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Responses;
using BookStoreEducation.DataAccessLayer.Models.ApplicationUser;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.BusinessLogicLayer.MappingProfiles
{
    public class UserManagmentProfile : Profile
    {
        public UserManagmentProfile()
        {
            CreateMap<GetUsersModel, UsersModel>()
                .ForMember(destin => destin.Users, opt => opt.MapFrom(src => src.Users));
            CreateMap<ApplicationUser, UsersModelItem>();
        }
    }
}
