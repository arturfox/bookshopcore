﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models.Catalog.Responses;
using BookStoreEducation.DataAccessLayer.Models.PrintingEdition;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.BusinessLogicLayer.MappingProfiles
{
    public class CatalogProfile : Profile
    {
        public CatalogProfile()
        {
            CreateMap<PrintingEdition, CatalogModelItem>()
                .ForMember(destin => destin.BaseCurrency, opt => opt.MapFrom(src => src.Currency))
                .ForMember(destin => destin.BasePrice, opt => opt.MapFrom(src => src.Price))
                .ForMember(destin => destin.ImageSource, opt => opt.MapFrom(src => src.ImageSource.ObjectUrl))
                .ForMember(destin => destin.Price, opt => opt.Ignore())
                .ForMember(destin => destin.Currency, opt => opt.Ignore());
            CreateMap<Author, AuthorModel>();
            CreateMap<GetPrintingEditionsModel, CatalogModel>()
                .ForMember(destin => destin.Items, opt => opt.MapFrom(src => src.PrintingEditions));

            CreateMap<PrintingEdition, CatalogDetailsModel>();
        }
    }
}
