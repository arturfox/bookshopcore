﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Responses;
using BookStoreEducation.DataAccessLayer.Models.PrintingEdition;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.BusinessLogicLayer.MappingProfiles
{
    public class CatalogManagmentProfile : Profile
    {
        public CatalogManagmentProfile()
        {
            CreateMap<AddCatalogItemModel, PrintingEdition>()
                .ForMember(destin => destin.ImageSource, opt => opt.Ignore());

            CreateMap<UpdateCatalogItemModel, PrintingEdition>()
                .ForMember(destin => destin.Id, opt => opt.Ignore())
                .ForMember(destin => destin.ImageSource, opt => opt.Ignore());

            CreateMap<PrintingEdition, CatalogModelItem>()
                .ForMember(destin => destin.ImageSource, opt => opt.MapFrom(src => src.ImageSource.ObjectUrl));
            CreateMap<Author, AuthorModel>();
            CreateMap<GetPrintingEditionsModel, CatalogModel>()
                .ForMember(destin => destin.Items, opt => opt.MapFrom(src => src.PrintingEditions));
        }
    } 
}
