﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models.Cart.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.Cart.Responses;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.BusinessLogicLayer.MappingProfiles
{
    public class CartProfile : Profile
    {
        public CartProfile()
        {
            CreateMap<PrintingEdition, CartModelItem>()
                .ForMember(destin => destin.PrintingEditionId, opt => opt.MapFrom(src => src.Id))
                .ForMember(destin => destin.ImageSource, opt => opt.MapFrom(src => src.ImageSource.ObjectUrl));
            CreateMap<AddPositionModel, CartPrintingEdition>()
                .ForMember(destin => destin.PrintingEditionId, opt => opt.MapFrom(src => src.PositionId));
        }
    }
}
