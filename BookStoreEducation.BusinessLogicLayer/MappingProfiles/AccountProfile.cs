﻿using AutoMapper;
using BookStoreEducation.BusinessLogicLayer.Models.Account.Responses;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.BusinessLogicLayer.MappingProfiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<ApplicationUser, ProfileModel>()
                .ForMember(destin => destin.ImageSource, opt => opt.MapFrom(src => src.ImageSource.ObjectUrl));
        }
    }   
}

