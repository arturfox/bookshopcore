﻿namespace BookStoreEducation.BusinessLogicLayer.Enums
{
    public enum BucketAccessoryFileType
    {
        None = 0,
        Catalog = 1,
        Profile = 2
    }
}
