﻿namespace BookStoreEducation.BusinessLogicLayer.Enums
{
    public partial class Enums
    {
        public enum SortType
        {
            None = 0,
            LowToHigh = 1,
            HighToLow = 2
        }
    }
}
