﻿
namespace BookStoreEducation.BusinessLogicLayer.Auth
{
    public static class Constants
    {
        public static class JwtClaimIdentifiers
        {
            public const string Role = "role";
            public const string Id = "id";
        }
    }
}
