﻿using BookStoreEducation.Entities.Entities;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BookStoreEducation.BusinessLogicLayer.Auth.Interfaces
{
    public interface IJwtFactory
    {
        Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity);
        ClaimsIdentity GenerateClaimsIdentity(ApplicationUser user, string userRole);
    }
}