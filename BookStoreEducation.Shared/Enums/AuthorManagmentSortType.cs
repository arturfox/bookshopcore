﻿namespace BookStoreEducation.Shared.Enums
{
    public partial class Enums
    {
        public enum AuthorManagmentSortType
        {
            None = 0,
            NumberDesc = 1,
            NumberAsc = 2,
            NameDesc = 3,
            NameAsc = 4
        }
    }
}
