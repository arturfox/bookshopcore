﻿using System.Threading.Tasks;

namespace BookStoreEducation.DataAccessLayer.AppContext.Interfaces
{
    public interface IDBIdentitySeeder
    {
        Task SeedData();
    }
}
