﻿using Microsoft.EntityFrameworkCore;

namespace BookStoreEducation.DataAccessLayer.AppContext.Interfaces
{
    public interface IDBSeeder
    {
        void SeedData(ModelBuilder builder);
    }
}
