﻿using BookStoreEducation.DataAccessLayer.AppContext.Interfaces;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace BookStoreEducation.DataAccessLayer.AppContext
{
    public class ApplicationContext : IdentityDbContext<ApplicationUser, IdentityRole<long>, long>
    {
        private readonly IDBSeeder _dBSeeder;

        public ApplicationContext(DbContextOptions<ApplicationContext> options,
                                  IDBSeeder dBSeeder)
            : base(options)
        {
            _dBSeeder = dBSeeder;
        }

        public DbSet<Author> Authors { get; set; }

        public DbSet<AuthorsInPrintingEdition> AuthorsInPrintingEditions { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderPosition> OrderPositions { get; set; }

        public DbSet<Payment> Payments { get; set; }

        public DbSet<PrintingEdition> PrintingEditions { get; set; }

        public DbSet<RefreshToken> RefreshTokens { get; set; }

        public DbSet<ImageSource> ImageSources { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            _dBSeeder.SeedData(builder);

            builder.Entity<Author>()
            .HasMany(p => p.PrintingEditions)
            .WithMany(p => p.Authors)
            .UsingEntity<AuthorsInPrintingEdition>(
                j => j
                    .HasOne(pt => pt.PrintingEdition)
                    .WithMany(t => t.AuthorsInPrintingEditions)
                    .HasForeignKey(pt => pt.PrintingEditionId),
                j => j
                    .HasOne(pt => pt.Author)
                    .WithMany(t => t.AuthorsInPrintingEditions)
                    .HasForeignKey(pt => pt.AuthorId)
             );

            builder.Entity<Cart>()
            .HasMany(p => p.PrintingEditions)
            .WithMany(p => p.Carts)
            .UsingEntity<CartPrintingEdition>(
                j => j
                    .HasOne(pt => pt.PrintingEdition)
                    .WithMany(t => t.CartPrintingEditions)
                    .HasForeignKey(pt => pt.PrintingEditionId),
                j => j
                    .HasOne(pt => pt.Cart)
                    .WithMany(t => t.CartPrintingEditions)
                    .HasForeignKey(pt => pt.CartId)
             );

            builder.Entity<ApplicationUser>().Property(au => au.AccountStatus).HasDefaultValue(AccountStatus.Active);

            base.OnModelCreating(builder);
        }
    }
}
