﻿using BookStoreEducation.DataAccessLayer.AppContext.Interfaces;
using BookStoreEducation.DataAccessLayer.Configurations;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BookStoreEducation.DataAccessLayer.AppContext
{
    public class DBIdentitySeeder : IDBIdentitySeeder
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole<long>> _roleManager;

        private readonly AdminCredentials _adminCredentials;

        public DBIdentitySeeder(IOptions<AdminCredentials> options,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole<long>> roleManager)
        {
            _adminCredentials = options.Value;

            _userManager = userManager;
            _roleManager = roleManager;
        }


        public async Task SeedData()
        {
            await SeedRoles();
            await SeedUsers();
        }

        private async Task SeedRoles()
        {
            if (_roleManager.Roles.Any())
            {
                return;
            }

            foreach (UserRole userRole in Enum.GetValues(typeof(UserRole)))
            {
                if (userRole == UserRole.None)
                {
                    continue;
                }

                var roleName = userRole.ToString();
                var role = new IdentityRole<long>(roleName);

                await _roleManager.CreateAsync(role);
            }
        }

        private async Task SeedUsers()
        {
            ApplicationUser user = await _userManager.FindByEmailAsync(_adminCredentials.Email);

            if (user != null)
            {
                return;
            }

            user = new ApplicationUser()
            {
                UserName = _adminCredentials.UserName,
                Email = _adminCredentials.Email,
                FirstName = _adminCredentials.FirstName,
                LastName = _adminCredentials.LastName,
                EmailConfirmed = true
            };

            IdentityResult result = await _userManager.CreateAsync(user, _adminCredentials.Password);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(user, UserRole.Admin.ToString());
            }
        }
    }
}
