﻿using BookStoreEducation.DataAccessLayer.AppContext.Interfaces;
using BookStoreEducation.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace BookStoreEducation.DataAccessLayer.AppContext
{
    public class DBSeeder : IDBSeeder
    {
        public void SeedData(ModelBuilder builder)
        {
            IEnumerable<Author> authors = CreateAuthors();
            builder.Entity<Author>().HasData(authors);

            IEnumerable<PrintingEdition> printingEditions = CreatePrintingEditions();
            builder.Entity<PrintingEdition>().HasData(printingEditions);

            IEnumerable<AuthorsInPrintingEdition> authorsInPrintingEdition = CreateAuthorsInPrintingEditions();
            builder.Entity<AuthorsInPrintingEdition>().HasData(authorsInPrintingEdition);
        }

        private IEnumerable<Author> CreateAuthors()
        {
            var authors = new List<Author>(new Author[] {
                new Author(){Id = 1, FirstName = "Walter", LastName = "Tevis", NormalizedFirstName = "WALTER", NormalizedLastName = "TEVIS"},
                new Author(){Id = 2, FirstName = "Anna", LastName = "Wintour", NormalizedFirstName = "ANNA", NormalizedLastName = "WINTOUR"},
                new Author(){Id = 3, FirstName = "Grace", LastName = "Mirabella", NormalizedFirstName = "GRACE", NormalizedLastName = "MIRABELLA"},
                new Author(){Id = 4, FirstName = "Yulia", LastName = "Mostova", NormalizedFirstName = "YULIA", NormalizedLastName = "MOSTOVA"}
            });

            return authors;
        }

        private IEnumerable<PrintingEdition> CreatePrintingEditions()
        {
            var printingEditions = new List<PrintingEdition>(new PrintingEdition[] {
                new PrintingEdition(){Id = 1, Currency = Entities.Enums.Currency.USD, Price = 11.4 , PrintingEditionType = Entities.Enums.PrintingEditionType.Book , Title = "The Queen's Gambit", NormalizedTitle = "THE QUEEN'S GAMBIT", Description = "Walter Tevis's most consummate and heartbreaking work -- Jonathan Lethem Tevis was a great storyteller -- Lionel Shriver Walter Tevis is famous for writing The Hustler and The Man Who Fell to Earth, but this is my favorite book of his. It is about a girl who, guided by her somewhat unreliable mother, becomes a child prodigy at chess. Even if you do not know how to play chess, it is a great thriller * Literary Hub * A psychological thriller * NEW YORK TIMES * What Walter Tevis did for pool in The Hustler, he does for chess in The Queen's Gambit * PLAYBOY * More exciting than any thriller I've seen lately; more than that, beautifully written Don't pick this up if you want a night's sleep * SCOTSMAN * Mesmerizing * NEWSWEEK * The Queen's Gambit is sheer entertainment. It is a book I reread every few years - for the pure pleasure and skill of it Gripping reading . . . Nabokov's The Defense and Zweig's The Royal Game are the classics. Now joining them is The Queen's Gambit * FINANCIAL TIMES * Superb * TIME OUT *" },
                new PrintingEdition(){Id = 2, Currency = Entities.Enums.Currency.USD, Price = 120 , PrintingEditionType = Entities.Enums.PrintingEditionType.Journal , Title = "Vogue", NormalizedTitle = "VOGUE", Description = "Vogue is an American monthly fashion and lifestyle magazine covering many topics, including fashion, beauty, culture, living, and runway, based in New York City. It began as a weekly newspaper, first published in New York City in 1892, before becoming a monthly publication years later." },
                new PrintingEdition(){Id = 3, Currency = Entities.Enums.Currency.USD, Price = 2.55 , PrintingEditionType = Entities.Enums.PrintingEditionType.Newspaper , Title = "Dzerkalo Tyzhnia", NormalizedTitle = "DZERKALO TYZHNIA", Description = "Dzerkalo Tyzhnia (Ukrainian: Дзеркало тижня), usually referred to in English as the Mirror Weekly, was one of Ukraine's most influential analytical weekly-publisher newspapers, founded in 1994. On 27 December 2019 it published its last printed issue, it continued its life as a Ukrainian news website" }
            });

            return printingEditions;
        }

        private IEnumerable<AuthorsInPrintingEdition> CreateAuthorsInPrintingEditions()
        {
            var authors = new List<AuthorsInPrintingEdition>(new AuthorsInPrintingEdition[] {
                new AuthorsInPrintingEdition(){Id = 1, AuthorId = 1, PrintingEditionId = 1},
                new AuthorsInPrintingEdition(){Id = 2, AuthorId = 2, PrintingEditionId = 2},
                new AuthorsInPrintingEdition(){Id = 3, AuthorId = 3, PrintingEditionId = 2},
                new AuthorsInPrintingEdition(){Id = 4, AuthorId = 4, PrintingEditionId = 3},
            });

            return authors;
        }
    }
}
