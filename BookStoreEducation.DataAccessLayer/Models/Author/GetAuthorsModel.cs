﻿using System.Collections.Generic;

namespace BookStoreEducation.DataAccessLayer.Models.Author
{
    public class GetAuthorsModel
    {
        public IEnumerable<Entities.Entities.Author> Authors { get; set; }
        public int TotalItems { get; set; }

        public GetAuthorsModel()
        {
            Authors = new List<Entities.Entities.Author>();
        }
    }
}
