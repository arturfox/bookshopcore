﻿using System.Collections.Generic;

namespace BookStoreEducation.DataAccessLayer.Models.PrintingEdition
{
    public class GetPrintingEditionsModel
    {
        public IEnumerable<Entities.Entities.PrintingEdition> PrintingEditions { get; set; }
        public int TotalItems { get; set; }

        public GetPrintingEditionsModel()
        {
            PrintingEditions = new List<Entities.Entities.PrintingEdition>();
        }
    }
}
