﻿using System.Collections.Generic;

namespace BookStoreEducation.DataAccessLayer.Models.Order
{
    public class GetOrdersModel
    {
        public IEnumerable<Entities.Entities.Order> Orders { get; set; }
        public int TotalItems { get; set; }

        public GetOrdersModel()
        {
            Orders = new List<Entities.Entities.Order>();
        }
    }
}
