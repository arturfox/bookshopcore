﻿using System.Collections.Generic;

namespace BookStoreEducation.DataAccessLayer.Models.ApplicationUser
{
    public class GetUsersModel
    {
        public IEnumerable<Entities.Entities.ApplicationUser> Users { get; set; }
        public int TotalItems { get; set; }

        public GetUsersModel()
        {
            Users = new List<Entities.Entities.ApplicationUser>();
        }
    }
}
