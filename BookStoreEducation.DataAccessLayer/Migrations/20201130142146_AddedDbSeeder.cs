﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStoreEducation.DataAccessLayer.Migrations
{
    public partial class AddedDbSeeder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "CreationDate", "FirstName", "IsRemoved", "LastName" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 11, 30, 14, 21, 46, 387, DateTimeKind.Utc).AddTicks(1705), "Walter", false, "Tevis" },
                    { 2L, new DateTime(2020, 11, 30, 14, 21, 46, 387, DateTimeKind.Utc).AddTicks(3599), "Anna", false, "Wintour" },
                    { 3L, new DateTime(2020, 11, 30, 14, 21, 46, 387, DateTimeKind.Utc).AddTicks(3635), "Grace", false, "Mirabella" },
                    { 4L, new DateTime(2020, 11, 30, 14, 21, 46, 387, DateTimeKind.Utc).AddTicks(3637), "Yulia", false, "Mostova" }
                });

            migrationBuilder.InsertData(
                table: "PrintingEditions",
                columns: new[] { "Id", "CreationDate", "Currency", "Description", "ImageSource", "IsRemoved", "Price", "PrintingEditionType", "Title" },
                values: new object[,]
                {
                    { 1L, new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(1186), 2, "Walter Tevis's most consummate and heartbreaking work -- Jonathan Lethem Tevis was a great storyteller -- Lionel Shriver Walter Tevis is famous for writing The Hustler and The Man Who Fell to Earth, but this is my favorite book of his. It is about a girl who, guided by her somewhat unreliable mother, becomes a child prodigy at chess. Even if you do not know how to play chess, it is a great thriller * Literary Hub * A psychological thriller * NEW YORK TIMES * What Walter Tevis did for pool in The Hustler, he does for chess in The Queen's Gambit * PLAYBOY * More exciting than any thriller I've seen lately; more than that, beautifully written Don't pick this up if you want a night's sleep * SCOTSMAN * Mesmerizing * NEWSWEEK * The Queen's Gambit is sheer entertainment. It is a book I reread every few years - for the pure pleasure and skill of it Gripping reading . . . Nabokov's The Defense and Zweig's The Royal Game are the classics. Now joining them is The Queen's Gambit * FINANCIAL TIMES * Superb * TIME OUT *", "https://lh3.googleusercontent.com/proxy/SzM96uoZowRJejzulk7rZxIimnXX-WnYl4CJhqfEWKA70njQt8COoDRnoczLvhJP6PXK4YBRc07KVftvG9jhKO-zKIdlmOXX5eUxOUxE_L3p2d0", false, 11.4, 1, "The Queen's Gambit" },
                    { 2L, new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(3843), 6, "Vogue is an American monthly fashion and lifestyle magazine covering many topics, including fashion, beauty, culture, living, and runway, based in New York City. It began as a weekly newspaper, first published in New York City in 1892, before becoming a monthly publication years later.", "https://journals-online.net/uploads/posts/2019-08/1566369403_vogue-9-2019.jpg", false, 120.0, 2, "Vogue" },
                    { 3L, new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(3903), 1, "Dzerkalo Tyzhnia (Ukrainian: Дзеркало тижня), usually referred to in English as the Mirror Weekly, was one of Ukraine's most influential analytical weekly-publisher newspapers, founded in 1994. On 27 December 2019 it published its last printed issue, it continued its life as a Ukrainian news website", "https://polvisti.com/wp-content/uploads/2019/11/hazeta.jpg", false, 2.5499999999999998, 1, "Dzerkalo Tyzhnia" }
                });

            migrationBuilder.InsertData(
                table: "AuthorsInPrintingEditions",
                columns: new[] { "Id", "AuthorId", "CreationDate", "IsRemoved", "PrintingEditionId" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(6907), false, 1L },
                    { 2L, 2L, new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(7798), false, 2L },
                    { 3L, 3L, new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(7815), false, 2L },
                    { 4L, 4L, new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(7817), false, 3L }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L);

            migrationBuilder.DeleteData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L);

            migrationBuilder.DeleteData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L);

            migrationBuilder.DeleteData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L);

            migrationBuilder.DeleteData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L);
        }
    }
}
