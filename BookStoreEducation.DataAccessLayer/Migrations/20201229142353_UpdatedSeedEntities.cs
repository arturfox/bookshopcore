﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStoreEducation.DataAccessLayer.Migrations
{
    public partial class UpdatedSeedEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 29, 14, 23, 52, 505, DateTimeKind.Utc).AddTicks(3201));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 29, 14, 23, 52, 505, DateTimeKind.Utc).AddTicks(6596));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 29, 14, 23, 52, 505, DateTimeKind.Utc).AddTicks(6658));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 29, 14, 23, 52, 505, DateTimeKind.Utc).AddTicks(6660));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 29, 14, 23, 52, 507, DateTimeKind.Utc).AddTicks(9901));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 29, 14, 23, 52, 508, DateTimeKind.Utc).AddTicks(783));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 29, 14, 23, 52, 508, DateTimeKind.Utc).AddTicks(807));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 29, 14, 23, 52, 508, DateTimeKind.Utc).AddTicks(808));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreationDate", "Currency", "ImageSource" },
                values: new object[] { new DateTime(2020, 12, 29, 14, 23, 52, 507, DateTimeKind.Utc).AddTicks(3768), 1, "https://lh3.googleusercontent.com/proxy/mWx0Y7OARJ-tKA38vqNcarYZNqVam7qUMMtuaYTI9hsIigZiiKajyKLcbz8x68IMih_Rn6vDmtgzOIWaScYCnUPUxubAnuRgFuKAUw80dh1iU5s" });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreationDate", "Currency" },
                values: new object[] { new DateTime(2020, 12, 29, 14, 23, 52, 507, DateTimeKind.Utc).AddTicks(7207), 1 });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 29, 14, 23, 52, 507, DateTimeKind.Utc).AddTicks(7263));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 9, 13, 17, 28, 612, DateTimeKind.Utc).AddTicks(2849));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 9, 13, 17, 28, 612, DateTimeKind.Utc).AddTicks(6282));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 9, 13, 17, 28, 612, DateTimeKind.Utc).AddTicks(6345));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 9, 13, 17, 28, 612, DateTimeKind.Utc).AddTicks(6348));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 9, 13, 17, 28, 615, DateTimeKind.Utc).AddTicks(745));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 9, 13, 17, 28, 615, DateTimeKind.Utc).AddTicks(1639));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 9, 13, 17, 28, 615, DateTimeKind.Utc).AddTicks(1677));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 9, 13, 17, 28, 615, DateTimeKind.Utc).AddTicks(1678));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreationDate", "Currency", "ImageSource" },
                values: new object[] { new DateTime(2020, 12, 9, 13, 17, 28, 614, DateTimeKind.Utc).AddTicks(4272), 2, "https://lh3.googleusercontent.com/proxy/SzM96uoZowRJejzulk7rZxIimnXX-WnYl4CJhqfEWKA70njQt8COoDRnoczLvhJP6PXK4YBRc07KVftvG9jhKO-zKIdlmOXX5eUxOUxE_L3p2d0" });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreationDate", "Currency" },
                values: new object[] { new DateTime(2020, 12, 9, 13, 17, 28, 614, DateTimeKind.Utc).AddTicks(7666), 6 });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 9, 13, 17, 28, 614, DateTimeKind.Utc).AddTicks(7739));
        }
    }
}
