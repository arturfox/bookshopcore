﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStoreEducation.DataAccessLayer.Migrations
{
    public partial class AddedImageSourceIdRefToApplicationUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "ImageSourceId",
                table: "AspNetUsers",
                type: "bigint",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 443, DateTimeKind.Utc).AddTicks(2243));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 443, DateTimeKind.Utc).AddTicks(5733));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 443, DateTimeKind.Utc).AddTicks(5804));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 443, DateTimeKind.Utc).AddTicks(5810));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 446, DateTimeKind.Utc).AddTicks(2039));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 446, DateTimeKind.Utc).AddTicks(2929));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 446, DateTimeKind.Utc).AddTicks(2956));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 446, DateTimeKind.Utc).AddTicks(2958));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 445, DateTimeKind.Utc).AddTicks(5522));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 445, DateTimeKind.Utc).AddTicks(8910));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 445, DateTimeKind.Utc).AddTicks(8966));

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_ImageSourceId",
                table: "AspNetUsers",
                column: "ImageSourceId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_ImageSources_ImageSourceId",
                table: "AspNetUsers",
                column: "ImageSourceId",
                principalTable: "ImageSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_ImageSources_ImageSourceId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_ImageSourceId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ImageSourceId",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 793, DateTimeKind.Utc).AddTicks(3190));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 793, DateTimeKind.Utc).AddTicks(7048));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 793, DateTimeKind.Utc).AddTicks(7127));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 793, DateTimeKind.Utc).AddTicks(7129));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 796, DateTimeKind.Utc).AddTicks(2253));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 796, DateTimeKind.Utc).AddTicks(3159));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 796, DateTimeKind.Utc).AddTicks(3191));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 796, DateTimeKind.Utc).AddTicks(3193));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 795, DateTimeKind.Utc).AddTicks(5804));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 795, DateTimeKind.Utc).AddTicks(9320));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 20, 51, 795, DateTimeKind.Utc).AddTicks(9380));
        }
    }
}
