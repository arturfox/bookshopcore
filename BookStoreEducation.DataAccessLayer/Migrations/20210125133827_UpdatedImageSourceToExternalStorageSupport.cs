﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStoreEducation.DataAccessLayer.Migrations
{
    public partial class UpdatedImageSourceToExternalStorageSupport : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Data",
                table: "ImageSources",
                newName: "Region");

            migrationBuilder.AddColumn<string>(
                name: "BucketName",
                table: "ImageSources",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Key",
                table: "ImageSources",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 768, DateTimeKind.Utc).AddTicks(954));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 768, DateTimeKind.Utc).AddTicks(4335));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 768, DateTimeKind.Utc).AddTicks(4400));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 768, DateTimeKind.Utc).AddTicks(4402));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 770, DateTimeKind.Utc).AddTicks(7864));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 770, DateTimeKind.Utc).AddTicks(8729));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 770, DateTimeKind.Utc).AddTicks(8755));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 770, DateTimeKind.Utc).AddTicks(8756));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 770, DateTimeKind.Utc).AddTicks(1668));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 770, DateTimeKind.Utc).AddTicks(5135));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 13, 38, 26, 770, DateTimeKind.Utc).AddTicks(5182));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BucketName",
                table: "ImageSources");

            migrationBuilder.DropColumn(
                name: "Key",
                table: "ImageSources");

            migrationBuilder.RenameColumn(
                name: "Region",
                table: "ImageSources",
                newName: "Data");

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 443, DateTimeKind.Utc).AddTicks(2243));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 443, DateTimeKind.Utc).AddTicks(5733));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 443, DateTimeKind.Utc).AddTicks(5804));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 443, DateTimeKind.Utc).AddTicks(5810));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 446, DateTimeKind.Utc).AddTicks(2039));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 446, DateTimeKind.Utc).AddTicks(2929));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 446, DateTimeKind.Utc).AddTicks(2956));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 446, DateTimeKind.Utc).AddTicks(2958));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 445, DateTimeKind.Utc).AddTicks(5522));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 445, DateTimeKind.Utc).AddTicks(8910));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 20, 10, 53, 55, 445, DateTimeKind.Utc).AddTicks(8966));
        }
    }
}
