﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStoreEducation.DataAccessLayer.Migrations
{
    public partial class UodatedSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreationDate", "NormalizedFirstName", "NormalizedLastName" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 17, 31, 11, DateTimeKind.Utc).AddTicks(5225), "WALTER", "TEVIS" });

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreationDate", "NormalizedFirstName", "NormalizedLastName" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 17, 31, 11, DateTimeKind.Utc).AddTicks(7975), "ANNA", "WINTOUR" });

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreationDate", "NormalizedFirstName", "NormalizedLastName" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 17, 31, 11, DateTimeKind.Utc).AddTicks(8031), "GRACE", "MIRABELLA" });

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "CreationDate", "NormalizedFirstName", "NormalizedLastName" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 17, 31, 11, DateTimeKind.Utc).AddTicks(8033), "YULIA", "MOSTOVA" });

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 17, 31, 13, DateTimeKind.Utc).AddTicks(7548));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 17, 31, 13, DateTimeKind.Utc).AddTicks(8257));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 17, 31, 13, DateTimeKind.Utc).AddTicks(8303));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 17, 31, 13, DateTimeKind.Utc).AddTicks(8304));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreationDate", "NormalizedTitle" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 17, 31, 13, DateTimeKind.Utc).AddTicks(2602), "THE QUEEN'S GAMBIT" });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreationDate", "NormalizedTitle" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 17, 31, 13, DateTimeKind.Utc).AddTicks(5145), "VOGUE" });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreationDate", "NormalizedTitle" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 17, 31, 13, DateTimeKind.Utc).AddTicks(5198), "DZERKALO TYZHNIA" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreationDate", "NormalizedFirstName", "NormalizedLastName" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 5, 4, 748, DateTimeKind.Utc).AddTicks(5922), null, null });

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreationDate", "NormalizedFirstName", "NormalizedLastName" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 5, 4, 748, DateTimeKind.Utc).AddTicks(7903), null, null });

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreationDate", "NormalizedFirstName", "NormalizedLastName" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 5, 4, 748, DateTimeKind.Utc).AddTicks(7956), null, null });

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "CreationDate", "NormalizedFirstName", "NormalizedLastName" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 5, 4, 748, DateTimeKind.Utc).AddTicks(7957), null, null });

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(7287));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(7923));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(7937));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(7938));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreationDate", "NormalizedTitle" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(2522), null });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreationDate", "NormalizedTitle" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(4928), null });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreationDate", "NormalizedTitle" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(4979), null });
        }
    }
}
