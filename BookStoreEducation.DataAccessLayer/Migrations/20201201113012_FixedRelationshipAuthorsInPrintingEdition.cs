﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStoreEducation.DataAccessLayer.Migrations
{
    public partial class FixedRelationshipAuthorsInPrintingEdition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 647, DateTimeKind.Utc).AddTicks(6851));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 647, DateTimeKind.Utc).AddTicks(9047));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 647, DateTimeKind.Utc).AddTicks(9079));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 647, DateTimeKind.Utc).AddTicks(9081));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 650, DateTimeKind.Utc).AddTicks(1998));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 650, DateTimeKind.Utc).AddTicks(2841));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 650, DateTimeKind.Utc).AddTicks(2860));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 650, DateTimeKind.Utc).AddTicks(2862));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 649, DateTimeKind.Utc).AddTicks(6454));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 649, DateTimeKind.Utc).AddTicks(9129));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 649, DateTimeKind.Utc).AddTicks(9187));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 387, DateTimeKind.Utc).AddTicks(1705));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 387, DateTimeKind.Utc).AddTicks(3599));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 387, DateTimeKind.Utc).AddTicks(3635));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 387, DateTimeKind.Utc).AddTicks(3637));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(6907));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(7798));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(7815));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(7817));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(1186));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(3843));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 11, 30, 14, 21, 46, 389, DateTimeKind.Utc).AddTicks(3903));
        }
    }
}
