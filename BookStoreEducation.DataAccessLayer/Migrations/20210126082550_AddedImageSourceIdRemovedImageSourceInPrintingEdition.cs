﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStoreEducation.DataAccessLayer.Migrations
{
    public partial class AddedImageSourceIdRemovedImageSourceInPrintingEdition : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageSource",
                table: "PrintingEditions");

            migrationBuilder.AddColumn<long>(
                name: "ImageSourceId",
                table: "PrintingEditions",
                type: "bigint",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 614, DateTimeKind.Utc).AddTicks(1111));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 614, DateTimeKind.Utc).AddTicks(4525));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 614, DateTimeKind.Utc).AddTicks(4598));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 614, DateTimeKind.Utc).AddTicks(4600));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 616, DateTimeKind.Utc).AddTicks(7765));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 616, DateTimeKind.Utc).AddTicks(8632));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 616, DateTimeKind.Utc).AddTicks(8659));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 616, DateTimeKind.Utc).AddTicks(8661));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 616, DateTimeKind.Utc).AddTicks(1897));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 616, DateTimeKind.Utc).AddTicks(4983));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 26, 8, 25, 49, 616, DateTimeKind.Utc).AddTicks(5046));

            migrationBuilder.CreateIndex(
                name: "IX_PrintingEditions_ImageSourceId",
                table: "PrintingEditions",
                column: "ImageSourceId");

            migrationBuilder.AddForeignKey(
                name: "FK_PrintingEditions_ImageSources_ImageSourceId",
                table: "PrintingEditions",
                column: "ImageSourceId",
                principalTable: "ImageSources",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PrintingEditions_ImageSources_ImageSourceId",
                table: "PrintingEditions");

            migrationBuilder.DropIndex(
                name: "IX_PrintingEditions_ImageSourceId",
                table: "PrintingEditions");

            migrationBuilder.DropColumn(
                name: "ImageSourceId",
                table: "PrintingEditions");

            migrationBuilder.AddColumn<string>(
                name: "ImageSource",
                table: "PrintingEditions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 15, 38, 58, 410, DateTimeKind.Utc).AddTicks(803));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 15, 38, 58, 410, DateTimeKind.Utc).AddTicks(4237));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 15, 38, 58, 410, DateTimeKind.Utc).AddTicks(4304));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 15, 38, 58, 410, DateTimeKind.Utc).AddTicks(4306));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 15, 38, 58, 412, DateTimeKind.Utc).AddTicks(8710));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 15, 38, 58, 412, DateTimeKind.Utc).AddTicks(9605));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 15, 38, 58, 412, DateTimeKind.Utc).AddTicks(9649));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2021, 1, 25, 15, 38, 58, 412, DateTimeKind.Utc).AddTicks(9650));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                columns: new[] { "CreationDate", "ImageSource" },
                values: new object[] { new DateTime(2021, 1, 25, 15, 38, 58, 412, DateTimeKind.Utc).AddTicks(2147), "https://lh3.googleusercontent.com/proxy/mWx0Y7OARJ-tKA38vqNcarYZNqVam7qUMMtuaYTI9hsIigZiiKajyKLcbz8x68IMih_Rn6vDmtgzOIWaScYCnUPUxubAnuRgFuKAUw80dh1iU5s" });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "CreationDate", "ImageSource" },
                values: new object[] { new DateTime(2021, 1, 25, 15, 38, 58, 412, DateTimeKind.Utc).AddTicks(5731), "https://journals-online.net/uploads/posts/2019-08/1566369403_vogue-9-2019.jpg" });

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreationDate", "ImageSource" },
                values: new object[] { new DateTime(2021, 1, 25, 15, 38, 58, 412, DateTimeKind.Utc).AddTicks(5785), "https://polvisti.com/wp-content/uploads/2019/11/hazeta.jpg" });
        }
    }
}
