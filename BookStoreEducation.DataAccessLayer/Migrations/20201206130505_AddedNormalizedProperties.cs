﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BookStoreEducation.DataAccessLayer.Migrations
{
    public partial class AddedNormalizedProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "NormalizedTitle",
                table: "PrintingEditions",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NormalizedFirstName",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NormalizedLastName",
                table: "Authors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 748, DateTimeKind.Utc).AddTicks(5922));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 748, DateTimeKind.Utc).AddTicks(7903));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 748, DateTimeKind.Utc).AddTicks(7956));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 748, DateTimeKind.Utc).AddTicks(7957));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(7287));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(7923));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(7937));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(7938));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(2522));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(4928));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreationDate", "PrintingEditionType" },
                values: new object[] { new DateTime(2020, 12, 6, 13, 5, 4, 750, DateTimeKind.Utc).AddTicks(4979), 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NormalizedTitle",
                table: "PrintingEditions");

            migrationBuilder.DropColumn(
                name: "NormalizedFirstName",
                table: "Authors");

            migrationBuilder.DropColumn(
                name: "NormalizedLastName",
                table: "Authors");

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 647, DateTimeKind.Utc).AddTicks(6851));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 647, DateTimeKind.Utc).AddTicks(9047));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 647, DateTimeKind.Utc).AddTicks(9079));

            migrationBuilder.UpdateData(
                table: "Authors",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 647, DateTimeKind.Utc).AddTicks(9081));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 650, DateTimeKind.Utc).AddTicks(1998));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 650, DateTimeKind.Utc).AddTicks(2841));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 650, DateTimeKind.Utc).AddTicks(2860));

            migrationBuilder.UpdateData(
                table: "AuthorsInPrintingEditions",
                keyColumn: "Id",
                keyValue: 4L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 650, DateTimeKind.Utc).AddTicks(2862));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 1L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 649, DateTimeKind.Utc).AddTicks(6454));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 2L,
                column: "CreationDate",
                value: new DateTime(2020, 12, 1, 11, 30, 11, 649, DateTimeKind.Utc).AddTicks(9129));

            migrationBuilder.UpdateData(
                table: "PrintingEditions",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "CreationDate", "PrintingEditionType" },
                values: new object[] { new DateTime(2020, 12, 1, 11, 30, 11, 649, DateTimeKind.Utc).AddTicks(9187), 1 });
        }
    }
}
