﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class RefreshTokenRepository : BaseRepository<RefreshToken>,
        IRefreshTokenRepository
    {
        public RefreshTokenRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<RefreshToken> GetRefreshToken(string refreshToken)
        {
            return  DbSet.AsQueryable().FirstOrDefault(token => token.Token == refreshToken);
        }
    }
}
