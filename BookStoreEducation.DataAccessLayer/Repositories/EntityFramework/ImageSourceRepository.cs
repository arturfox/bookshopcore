﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class ImageSourceRepository : BaseRepository<ImageSource>,
        IImageSourceRepository
    {
        public ImageSourceRepository(ApplicationContext context) : base(context)
        {
        }
    }
}