﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class OrderPositionRepository : BaseRepository<OrderPosition>,
        IOrderPositionRepository
    {
        public OrderPositionRepository(ApplicationContext context) : base(context)
        {
        }
    }
}