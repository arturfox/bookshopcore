﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Models.ApplicationUser;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class ApplicationUserRepository : BaseRepository<ApplicationUser>,
        IApplicationUserRepository
    {
        public ApplicationUserRepository(ApplicationContext context) : base(context)
        {
        }

        public Task<ApplicationUser> GetUserById(long Id)
        {
            return DbSet.AsQueryable()
                        .Include(user => user.ImageSource)
                        .FirstOrDefaultAsync(user => user.Id == Id);
        }

        public async Task<GetUsersModel> GetUsers(int take, int offset, UserManagmentSortType sortBy, 
            IEnumerable<AccountStatus> statuses = null, IEnumerable<string> keywords = null)
        {
            IQueryable<ApplicationUser> existingUsers = DbSet.AsQueryable()
                                   .Where(applicationUser => !applicationUser.IsRemoved);

            if (statuses != null && statuses.Any())
            {
                existingUsers = existingUsers.Where(applicationUser
                            => statuses.Contains(applicationUser.AccountStatus));
            }

            IOrderedQueryable<ApplicationUser> sortedUsers = GetOrderedBySortTypeUsers(existingUsers, sortBy);


            //optimized - <not fetch 'all' users for search> if keywords is null
            if (keywords == null || !keywords.Any())
            {
                var totalCount = sortedUsers.Count();
                List<ApplicationUser> applicationUsers = await sortedUsers.Skip(offset)
                                                                          .Take(take)
                                                                          .ToListAsync();

                var model = new GetUsersModel()
                {
                    TotalItems = totalCount,
                    Users = applicationUsers
                };

                return model;
            }

            List<ApplicationUser> users = (await sortedUsers.ToListAsync())
                                          .Where(applicationUser
                                             => keywords.Any(keyword => applicationUser.NormalizedFirstName.Contains(keyword)
                                                                        || applicationUser.NormalizedLastName.Contains(keyword)
                                                                        || applicationUser.NormalizedEmail.Contains(keyword)))
                                          .ToList();

            var resultModel = new GetUsersModel()
            {
                TotalItems = users.Count,
                Users = users
            };

            return resultModel;

        }

        private IOrderedQueryable<ApplicationUser> GetOrderedBySortTypeUsers(IQueryable<ApplicationUser> existingUsers, UserManagmentSortType sortBy)
        {
            if (sortBy == UserManagmentSortType.UserNameDesc)
            {
                return existingUsers.OrderByDescending(applicationUser => applicationUser.UserName);
            }

            if (sortBy == UserManagmentSortType.UserNameAsc)
            {
                return existingUsers.OrderBy(applicationUser => applicationUser.UserName);
            }

            if (sortBy == UserManagmentSortType.EmailDesc)
            {
                return existingUsers.OrderByDescending(applicationUser => applicationUser.Email);
            }

            if (sortBy == UserManagmentSortType.EmailAsc)
            {
                return existingUsers.OrderBy(applicationUser => applicationUser.Email);
            }

            return existingUsers.OrderByDescending(applicationUser => applicationUser.Id);
        }
    }
}