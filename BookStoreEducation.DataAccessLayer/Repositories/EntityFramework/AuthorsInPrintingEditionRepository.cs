﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class AuthorsInPrintingEditionRepository : BaseRepository<AuthorsInPrintingEdition>,
        IAuthorsInPrintingEditionRepository
    {
        public AuthorsInPrintingEditionRepository(ApplicationContext context) : base(context)
        {
        }
    }
}