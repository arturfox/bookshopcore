﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Models.Author;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class AuthorRepository : BaseRepository<Author>, IAuthorRepository
    {
        public AuthorRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<Author> GetAuthorById(long id)
        {
            var author = await DbSet.AsQueryable()
                                 .Where(author => author.Id == id)
                                 .Include(author => author.PrintingEditions)
                                 .FirstOrDefaultAsync();

            return author;
        }

        public async Task<GetAuthorsModel> GetAuthors(int take, int offset, AuthorManagmentSortType sortType)
        {
            IQueryable<Author> activeAuthors = DbSet.AsQueryable()
                                                    .Where(author => !author.IsRemoved);

            IOrderedQueryable<Author> sortedAuthors = GetOrderedBySortTypeAuthors(activeAuthors, sortType);

            List<Author> authors = await sortedAuthors.Include(author => author.PrintingEditions)
                                                      .Skip(offset)
                                                      .Take(take)
                                                      .ToListAsync();

            var model = new GetAuthorsModel()
            {
                TotalItems = sortedAuthors.Count(),
                Authors = authors
            };

            return model;
        }

        public Task<List<Author>> GetAll()
        {
            return DbSet.AsQueryable()
                        .Where(author => !author.IsRemoved)
                        .ToListAsync();
        }

        private IOrderedQueryable<Author> GetOrderedBySortTypeAuthors(IQueryable<Author> authors, AuthorManagmentSortType sortBy)
        {
            if (sortBy == AuthorManagmentSortType.NumberDesc)
            {
                return authors.OrderByDescending(author => author.Id);
            }

            if (sortBy == AuthorManagmentSortType.NumberAsc)
            {
                return authors.OrderBy(author => author.Id);
            }

            if (sortBy == AuthorManagmentSortType.NameDesc)
            {
                return authors.OrderByDescending(author => author.NormalizedLastName)
                              .OrderByDescending(author => author.NormalizedFirstName);
            }

            if (sortBy == AuthorManagmentSortType.NameAsc)
            {
                return authors.OrderBy(author => author.NormalizedLastName)
                              .OrderBy(author => author.NormalizedFirstName);
            }

            return authors.OrderByDescending(author => author.Id);
        }
    }
}