﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class CartPrintingEditionRepository : BaseRepository<CartPrintingEdition>
        , ICartPrintingEditionRepository
    {
        public CartPrintingEditionRepository(ApplicationContext context) : base(context)
        {
        }
    }
}
