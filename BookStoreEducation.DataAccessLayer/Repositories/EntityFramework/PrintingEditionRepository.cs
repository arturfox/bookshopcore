﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Models.PrintingEdition;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class PrintingEditionRepository : BaseRepository<PrintingEdition>,
        IPrintingEditionRepository
    {
        public PrintingEditionRepository(ApplicationContext context) 
            : base(context)
        {
        }

        public async Task<GetPrintingEditionsModel> GetPrintingEditions(int take, int offset, int? minValue, int? maxValue, bool sortByPriceDesc = true, 
            IEnumerable<PrintingEditionType> types = null, IEnumerable<string> keywords = null)
        {
            IQueryable<PrintingEdition> activePrintingEditions = DbSet.AsQueryable()
                                               .Where(printingEdition => !printingEdition.IsRemoved);

            if (types != null && types.Any())
            {
                activePrintingEditions = activePrintingEditions.Where(printingEdition 
                            => types.Contains(printingEdition.PrintingEditionType));
            }

            if (minValue != null)
            {
                activePrintingEditions = activePrintingEditions.Where(printingEdition => printingEdition.Price >= minValue);
            }

            if(maxValue != null)
            {
                activePrintingEditions = activePrintingEditions.Where(printingEdition => printingEdition.Price <= maxValue);
            }

            IOrderedQueryable<PrintingEdition> sortedPrintingEditions = 
                sortByPriceDesc ? activePrintingEditions.OrderByDescending(printingEdition => printingEdition.Price) 
                                : activePrintingEditions.OrderBy(printingEdition => printingEdition.Price);

            List<PrintingEdition> printingEditions = await sortedPrintingEditions.Include(pritingEdition => pritingEdition.Authors)
                                                                                 .Include(pritingEdition => pritingEdition.ImageSource)
                                                                                 .ToListAsync();
            if (keywords != null && keywords.Any())
            {
                printingEditions = printingEditions.Where(printingEdition
                                             => keywords.Any(keyword => printingEdition.NormalizedTitle.Contains(keyword)
                                                                     || (printingEdition.Authors != null 
                                                                        && printingEdition.Authors.Any(author => author.NormalizedFirstName.Contains(keyword)
                                                                                                                 || author.NormalizedLastName.Contains(keyword)))))
                                                   .ToList();
            }

            var model = new GetPrintingEditionsModel()
            {
                TotalItems = printingEditions.Count
            };

            model.PrintingEditions = printingEditions.Skip(offset)
                                                     .Take(take)
                                                     .ToList();

            return model;
        }

        public async Task<GetPrintingEditionsModel> GetPrintingEditions(int take, int offset, CatalogManagmentSortType sortType, IEnumerable<PrintingEditionType> types = null)
        {
            IQueryable<PrintingEdition> activePrintingEditions = DbSet.AsQueryable()
                                                 .Where(printingEdition => !printingEdition.IsRemoved);

            if (types != null && types.Any())
            {
                activePrintingEditions = activePrintingEditions.Where(printingEdition
                            => types.Contains(printingEdition.PrintingEditionType));
            }

            IOrderedQueryable<PrintingEdition> sortedPrintingEditions = GetOrderedBySortTypePrintingEditions(activePrintingEditions, sortType);

            List<PrintingEdition> printingEditions = await sortedPrintingEditions.Include(pritingEdition => pritingEdition.Authors)
                                                                                 .Include(pritingEdition => pritingEdition.ImageSource)
                                                                                 .Skip(offset)
                                                                                 .Take(take)
                                                                                 .ToListAsync();

            var model = new GetPrintingEditionsModel()
            {
                TotalItems = sortedPrintingEditions.Count(),
                PrintingEditions = printingEditions
            };

            return model;
        }

        public async Task<PrintingEdition> GetPrintingEditionById(long id)
        {
           var printingEdition = await DbSet.AsQueryable()
                                            .Where(printingEdition => printingEdition.Id == id)
                                            .Include(pritingEdition => pritingEdition.Authors)
                                            .Include(pritingEdition => pritingEdition.ImageSource)
                                            .FirstOrDefaultAsync();

            return printingEdition;
        }

        public async Task<PrintingEdition> GetPrintingEditionByIdDetailed(long id)
        {
            var printingEdition = await DbSet.AsQueryable()
                                            .Where(printingEdition => printingEdition.Id == id)
                                            .Include(pritingEdition => pritingEdition.Authors)
                                            .Include(pritingEdition => pritingEdition.CartPrintingEditions)
                                            .Include(pritingEdition => pritingEdition.ImageSource)
                                            .FirstOrDefaultAsync();

            return printingEdition;
        }

        private IOrderedQueryable<PrintingEdition> GetOrderedBySortTypePrintingEditions(IQueryable<PrintingEdition> printingEditions, CatalogManagmentSortType sortBy)
        {
            if (sortBy == CatalogManagmentSortType.NumberDesc)
            {
                return printingEditions.OrderByDescending(printingEdition => printingEdition.Id);
            }

            if (sortBy == CatalogManagmentSortType.NumberAsc)
            {
                return printingEditions.OrderBy(printingEdition => printingEdition.Id);
            }

            if (sortBy == CatalogManagmentSortType.NameDesc)
            {
                return printingEditions.OrderByDescending(printingEdition => printingEdition.NormalizedTitle);
            }

            if (sortBy == CatalogManagmentSortType.NameAsc)
            {
                return printingEditions.OrderBy(printingEdition => printingEdition.NormalizedTitle);
            }

            if (sortBy == CatalogManagmentSortType.PriceAsc)
            {
                return printingEditions.OrderBy(printingEdition => printingEdition.Price);
            }

            if (sortBy == CatalogManagmentSortType.PriceDesc)
            {
                return printingEditions.OrderByDescending(printingEdition => printingEdition.Price);
            }

            return printingEditions.OrderByDescending(printingEdition => printingEdition.Id);
        }
    }
}