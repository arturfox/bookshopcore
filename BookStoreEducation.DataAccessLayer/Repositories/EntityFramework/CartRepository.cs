﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class CartRepository : BaseRepository<Cart>, ICartRepository
    {
        public CartRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<Cart> GetCartByUserId(long userId)
        {
            var cart = await DbSet.AsQueryable()
                                            .Where(cart => cart.UserId == userId)
                                            .Include(cart => cart.CartPrintingEditions)
                                            .Include(cart => cart.PrintingEditions)
                                            .FirstOrDefaultAsync();
            return cart;
        }
    }
}
