﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Models.Order;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using BookStoreEducation.Shared.Enums;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Order>> GetOrdersByUserId(long userId)
        {
            List<Order> orders = await DbSet.AsQueryable()
                                            .Where(order => order.UserId == userId && !order.IsRemoved)
                                            .Include(order => order.Payment)
                                            .Include(order => order.OrderPositions)
                                                .ThenInclude(position => position.PrintingEdition)
                                                    .ThenInclude(printingEdition => printingEdition.ImageSource)
                                            .OrderBy(order => order.OrderStatus)
                                            .ToListAsync();

            return orders;
        }

        public async Task<Order> GetOrderById(long orderId)
        {
            var order = await DbSet.AsQueryable()
                                    .Where(order => order.Id == orderId && !order.IsRemoved)
                                    .Include(order => order.Payment)
                                    .Include(order => order.OrderPositions)
                                        .ThenInclude(position => position.PrintingEdition)
                                            .ThenInclude(printingEdition => printingEdition.ImageSource)
                                     .OrderBy(order => order.OrderStatus)
                                     .FirstOrDefaultAsync();

            return order;
        }

        public async Task<GetOrdersModel> GetOrders(int take, int offset, Enums.OrderManagmentSortType sortBy, IEnumerable<OrderStatus> statuses = null)
        {
            IQueryable<Order> existingOrders = DbSet.AsQueryable()
                                  .Where(order => !order.IsRemoved);

            if (statuses != null && statuses.Any())
            {
                existingOrders = existingOrders.Where(order
                            => statuses.Contains(order.OrderStatus));
            }
          
            IOrderedQueryable<Order> sortedOrders = GetOrderedBySortTypeOrders(existingOrders, sortBy);
            var totalCount = sortedOrders.Count();

            List<Order> orders = await sortedOrders.Skip(offset)
                                                   .Take(take)
                                                   .Include(order => order.User)
                                                   .Include(order => order.Payment)
                                                   .Include(order => order.OrderPositions)
                                                        .ThenInclude(position => position.PrintingEdition)
                                                            .ThenInclude(printingEdition => printingEdition.ImageSource)
                                                   .ToListAsync();

            var model = new GetOrdersModel()
            {
                TotalItems = totalCount,
                Orders = orders
            };

            return model;
        }

        private IOrderedQueryable<Order> GetOrderedBySortTypeOrders(IQueryable<Order> existingOrders, OrderManagmentSortType sortBy)
        {
            if (sortBy == OrderManagmentSortType.DateDesc)
            {
                return existingOrders.OrderByDescending(order => order.CreationDate);
            }

            if (sortBy == OrderManagmentSortType.DateAsc)
            {
                return existingOrders.OrderBy(order => order.CreationDate);
            }

            if (sortBy == OrderManagmentSortType.NumberDesc)
            {
                return existingOrders.OrderByDescending(order => order.Id);
            }

            if (sortBy == OrderManagmentSortType.NumberAsc)
            {
                return existingOrders.OrderBy(order => order.Id);
            }

            return existingOrders.OrderByDescending(applicationUser => applicationUser.Id);
        }
    }
}