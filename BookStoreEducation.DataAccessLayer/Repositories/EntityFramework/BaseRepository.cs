﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStoreEducation.DataAccessLayer.Repositories.EntityFramework
{
    public abstract class BaseRepository<TEntity>  : IBaseRepository<TEntity> where TEntity : class, IBaseEntity
                                                                                         
    {
        private ApplicationContext _сontext;

        protected DbSet<TEntity> DbSet { get; set; }

        public BaseRepository(ApplicationContext context)
        {
            _сontext = context;
            DbSet = _сontext.Set<TEntity>();
        }

        public async Task AddAsync(TEntity entity)
        {
            await DbSet.AddAsync(entity);
            await SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            await DbSet.AddRangeAsync(entities);
            await SaveChangesAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAsync()
        {
            return await DbSet.ToListAsync();
        }

        public IQueryable<TEntity> Get()
        {
            return DbSet;
        }

        public async Task<TEntity> GetAsync(long id)
        {
            return await DbSet.AsQueryable()
                               //.AsNoTracking()
                               .FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<TEntity>> GetRangeAsync(IEnumerable<long> entityIdList)
        {
            return await DbSet.AsQueryable()
                .Where(x => entityIdList.Contains(x.Id))
                .ToListAsync();
        }

        public async Task UpdateAsync(TEntity entity)
        {
            DbSet.Update(entity);
            await SaveChangesAsync();
        }

        public async Task RemoveAsync(TEntity entity)
        {
            DbSet.Remove(entity);
            await SaveChangesAsync();
        }

        public async Task RemoveAsync(long id)
        {
            TEntity entity = await GetAsync(id);
            await RemoveAsync(entity);
        }

        public async Task RemoveRangeAsync(IEnumerable<TEntity> entities)
        {
            DbSet.RemoveRange(entities);
            await SaveChangesAsync();
        }

        public async Task RemoveRangeAsync(IEnumerable<long> entityIdList)
        {
            IEnumerable<TEntity> entities = await GetRangeAsync(entityIdList);

            await RemoveRangeAsync(entities);
        }

        public async Task UpdateRangeAsync(IEnumerable<TEntity> entities)
        {
            DbSet.UpdateRange(entities);
            await SaveChangesAsync();
        }

        protected async Task SaveChangesAsync()
        {
            await _сontext.SaveChangesAsync();
        }
    }
}