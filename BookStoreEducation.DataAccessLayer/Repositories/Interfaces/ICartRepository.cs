﻿using BookStoreEducation.Entities.Entities;
using System.Threading.Tasks;

namespace BookStoreEducation.DataAccessLayer.Repositories.Interfaces
{
    public interface ICartRepository : IBaseRepository<Cart>
    {
        Task<Cart> GetCartByUserId(long userId);
    }
}
