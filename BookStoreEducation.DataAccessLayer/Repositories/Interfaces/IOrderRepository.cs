﻿using BookStoreEducation.DataAccessLayer.Models.Order;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.DataAccessLayer.Repositories.Interfaces
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        Task<IEnumerable<Order>> GetOrdersByUserId(long userId);
        Task<Order> GetOrderById(long orderId);
        Task<GetOrdersModel> GetOrders(int take, int offset, OrderManagmentSortType sortBy,
            IEnumerable<OrderStatus> statuses = null);
    }
}