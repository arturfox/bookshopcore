﻿using BookStoreEducation.Entities.Entities;
using System.Threading.Tasks;

namespace BookStoreEducation.DataAccessLayer.Repositories.Interfaces
{
    public interface IRefreshTokenRepository : IBaseRepository<RefreshToken>
    {
        Task<RefreshToken> GetRefreshToken(string refreshToken);
    }
}
