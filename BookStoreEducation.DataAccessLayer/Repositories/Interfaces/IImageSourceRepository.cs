﻿using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.DataAccessLayer.Repositories.Interfaces
{
    public interface IImageSourceRepository : IBaseRepository<ImageSource>
    {
    }
}
