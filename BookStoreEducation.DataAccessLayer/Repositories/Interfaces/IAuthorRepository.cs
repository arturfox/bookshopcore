﻿using BookStoreEducation.DataAccessLayer.Models.Author;
using BookStoreEducation.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorRepository : IBaseRepository<Author>
    {
        Task<Author> GetAuthorById(long id);
        Task<GetAuthorsModel> GetAuthors(int take, int offset, AuthorManagmentSortType sortType);
        Task<List<Author>>GetAll();
    }
}