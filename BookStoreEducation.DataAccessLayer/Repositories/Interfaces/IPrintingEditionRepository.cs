﻿using BookStoreEducation.DataAccessLayer.Models.PrintingEdition;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.DataAccessLayer.Repositories.Interfaces
{
    public interface IPrintingEditionRepository : IBaseRepository<PrintingEdition>
    {
        Task<GetPrintingEditionsModel> GetPrintingEditions(int take, int offset, int? minValue, int? maxValue, bool sortByPriceDesc = true,
            IEnumerable<PrintingEditionType> types = null, IEnumerable<string> keywords = null);
        Task<GetPrintingEditionsModel> GetPrintingEditions(int take, int offset, CatalogManagmentSortType sortType,
            IEnumerable<PrintingEditionType> types = null);
        Task<PrintingEdition> GetPrintingEditionById(long id);
        Task<PrintingEdition> GetPrintingEditionByIdDetailed(long id);
    }
}