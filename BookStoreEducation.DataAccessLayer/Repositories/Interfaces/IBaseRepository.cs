﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStoreEducation.DataAccessLayer.Repositories.Interfaces
{
	public interface IBaseRepository<TEntity>
	{
		Task AddAsync(TEntity entity);

		Task AddRangeAsync(IEnumerable<TEntity> entities);

		Task<IEnumerable<TEntity>> GetAsync();

		Task<TEntity> GetAsync(long id);

		IQueryable<TEntity> Get();

		Task<IEnumerable<TEntity>> GetRangeAsync(IEnumerable<long> entityIdList);

		Task UpdateAsync(TEntity entity);

		Task RemoveAsync(TEntity entity);

		Task RemoveAsync(long id);

		Task RemoveRangeAsync(IEnumerable<TEntity> entities);

		Task RemoveRangeAsync(IEnumerable<long> entityIdList);

		Task UpdateRangeAsync(IEnumerable<TEntity> entities);
	}
}
