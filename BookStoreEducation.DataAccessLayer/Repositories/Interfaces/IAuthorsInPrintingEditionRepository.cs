﻿using BookStoreEducation.Entities.Entities;

namespace BookStoreEducation.DataAccessLayer.Repositories.Interfaces
{
    public interface IAuthorsInPrintingEditionRepository : IBaseRepository<AuthorsInPrintingEdition>
    {
    }
}
