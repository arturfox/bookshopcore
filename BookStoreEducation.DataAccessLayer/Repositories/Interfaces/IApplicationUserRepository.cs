﻿using BookStoreEducation.DataAccessLayer.Models.ApplicationUser;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.DataAccessLayer.Repositories.Interfaces
{
    public interface IApplicationUserRepository : IBaseRepository<ApplicationUser>
    {
        Task<GetUsersModel> GetUsers(int take, int offset, UserManagmentSortType sortBy, 
            IEnumerable<AccountStatus> statuses = null, IEnumerable<string> keywords = null);
        Task<ApplicationUser> GetUserById(long Id);
    }
}