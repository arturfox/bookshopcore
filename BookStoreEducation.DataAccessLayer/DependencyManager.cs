﻿using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.AppContext.Interfaces;
using BookStoreEducation.DataAccessLayer.Configurations;
using BookStoreEducation.DataAccessLayer.Repositories.EntityFramework;
using BookStoreEducation.DataAccessLayer.Repositories.Interfaces;
using BookStoreEducation.Entities.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStoreEducation.DataAccessLayer
{
    public class DependencyManager
    {
        public static void Configure(IServiceCollection services, IConfiguration configuration)
        {
            string connection = configuration.GetSection("ConnectionStrings:SQLServerConnection").Value;

            services.Configure<AdminCredentials>(configuration.GetSection("AdminCredentials"));

            services.AddDbContext<ApplicationContext>(options => 
                options.UseSqlServer(connection));

            services.AddIdentity<ApplicationUser, IdentityRole<long>>().AddEntityFrameworkStores<ApplicationContext>();

            services.AddTransient<IApplicationUserRepository, ApplicationUserRepository>();
            services.AddTransient<IAuthorRepository, AuthorRepository>();
            services.AddTransient<IAuthorsInPrintingEditionRepository, AuthorsInPrintingEditionRepository>();
            services.AddTransient<IOrderPositionRepository, OrderPositionRepository>();
            services.AddTransient<IOrderRepository, OrderRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<IPrintingEditionRepository, PrintingEditionRepository>();
            services.AddTransient<IRefreshTokenRepository, RefreshTokenRepository>();
            services.AddTransient<ICartRepository, CartRepository>();
            services.AddTransient<ICartPrintingEditionRepository, CartPrintingEditionRepository>();
            services.AddTransient<IImageSourceRepository, ImageSourceRepository>();

            services.AddTransient<IDBSeeder, DBSeeder>();
            services.AddTransient<IDBIdentitySeeder, DBIdentitySeeder>();
        }
    }
}
