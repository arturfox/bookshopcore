﻿using BookStoreEducation.BusinessLogicLayer.Models;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace BookStoreEducation.PresentationLayer.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(context, exception);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var resultModel = new ResponseModel()
            {
                IsSucceed = false,
                StatusCode = (int)HttpStatusCode.InternalServerError
            };

            resultModel.Errors.Add(exception.ToString());

            context.Response.StatusCode = (int)HttpStatusCode.OK;
            var result = JsonConvert.SerializeObject(resultModel);
            context.Response.ContentType = @"application/json";

            return context.Response.WriteAsync(result);
        }
    }
}
