﻿using BookStoreEducation.BusinessLogicLayer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStoreEducation.PresentationLayer.Configuration
{
    public static class DependencyExtention
    {
        public static void ConfigureDependency(this IServiceCollection services, IConfiguration configuration)
        {
            DependencyManager.Configure(services, configuration);
        }
    }
}
