﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStoreEducation.PresentationLayer.Configuration
{
    public static class CorsExtention
    {
        public static void ConfigureCors(this IServiceCollection services, IConfiguration configuration)
        {
            var corsOptions = configuration.GetSection("Cors");
            var origins = corsOptions["Origins"];
            services.AddCors(options =>
            {
                options.AddPolicy("AllowAllPolicy",
                    b => b.AllowAnyHeader()
                          .AllowAnyMethod()
                          .AllowAnyOrigin());

                options.AddPolicy("OriginPolicy",
                  b => b.WithOrigins(origins)
                        .AllowAnyHeader()
                        .AllowAnyMethod());
            });
        }
    }
}
