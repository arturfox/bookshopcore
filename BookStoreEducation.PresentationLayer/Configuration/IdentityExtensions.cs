﻿using BookStoreEducation.BusinessLogicLayer.Common.Constants;
using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.Entities.Entities;
using BookStoreEducation.PresentationLayer.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BookStoreEducation.PresentationLayer.Configuration
{
    public static class IdentityExtention
    {
        public static void ConfigureIdentity(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<EmailConfirmationTokenProviderOptions>(options =>
            {
                options.TokenLifespan = Constants.Authentication.EmailConfirmationTokenLifespan;
            });

            var builder = services.AddIdentityCore<ApplicationUser>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequireLowercase = true;
                options.Password.RequireUppercase = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequiredLength = 6;
                options.SignIn.RequireConfirmedEmail = true;
                options.User.RequireUniqueEmail = true;
                options.Tokens.EmailConfirmationTokenProvider = "emailconf";
            })
                .AddTokenProvider<EmailConfirmationTokenProvider<ApplicationUser>>("emailconf");

            builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole<long>), builder.Services);
            builder.AddEntityFrameworkStores<ApplicationContext>().AddDefaultTokenProviders();
        }
    }
}
