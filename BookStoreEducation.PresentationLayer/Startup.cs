using BookStoreEducation.BusinessLogicLayer.Configurations;
using BookStoreEducation.DataAccessLayer.AppContext;
using BookStoreEducation.DataAccessLayer.AppContext.Interfaces;
using BookStoreEducation.PresentationLayer.Configuration;
using BookStoreEducation.PresentationLayer.Filters;
using BookStoreEducation.PresentationLayer.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Stripe;

namespace BookStoreEducation.PresentationLayer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureDependency(Configuration);
            services.ConfigureCors(Configuration);
            services.ConfigureIdentity(Configuration);
            services.ConfigureAuthentication(Configuration);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });
            services.AddControllers(config => {

                config.Filters.Add<ModelStateValidationFilter>();
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<ApplicationContext>();
                context.Database.Migrate();

                var dBSeeder = serviceScope.ServiceProvider.GetRequiredService<IDBIdentitySeeder>();
                dBSeeder.SeedData().GetAwaiter().GetResult();

                var stripeOptions = serviceScope.ServiceProvider.GetRequiredService<IOptions<StripeOptions>>();
                StripeConfiguration.ApiKey = stripeOptions.Value.SecretKey;
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseCors("AllowAllPolicy");

            //disabled to test stripe webhooks
            if (!env.IsDevelopment())
            {
                app.UseHttpsRedirection();
            } 

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
