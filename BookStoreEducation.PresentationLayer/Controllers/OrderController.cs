﻿using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Order.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStoreEducation.PresentationLayer.Controllers
{
    [Authorize(Roles = "Client")]
    [ApiController]
    [Route("[controller]")]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet("get")]
        public async Task<ActionResult<ResponseModel<OrdersModel>>> GetOrders()
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _orderService.GetOrders(userId.Value);
            return Ok(result);
        }
    }
}
