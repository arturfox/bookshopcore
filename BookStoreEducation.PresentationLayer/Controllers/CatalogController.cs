﻿using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Catalog.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using BookStoreEducation.Entities.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static BookStoreEducation.BusinessLogicLayer.Common.Constants.Constants;
using static BookStoreEducation.BusinessLogicLayer.Enums.Enums;

namespace BookStoreEducation.PresentationLayer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CatalogController : ControllerBase
    {
        private readonly ICatalogService _catalogService;

        public CatalogController(ICatalogService catalogService)
        {
            _catalogService = catalogService;
        }

        [HttpGet]
        public string Get()
        {
            return "You are on shop page";
        }

        [AllowAnonymous]
        [HttpGet("getList")]
        public async Task<ActionResult<ResponseModel<CatalogModel>>> GetList(string searchQuery, string categories, int? minValue, 
            int? maxValue, int takeItems = Pagination.Catalog.ItemsCountPerPage, int page = 0, SortType orderBy = SortType.LowToHigh, Currency currency = Currency.USD)
        {
            var result = await _catalogService.GetCatalog(searchQuery, categories, minValue, maxValue, takeItems, page, orderBy, currency);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("details")]
        public async Task<ActionResult<ResponseModel<CatalogDetailsModel>>> GetDetails(int printingEditionId)
        {
            var result = await _catalogService.GetCatalogDetails(printingEditionId);
            return Ok(result);
        }
    }
}
