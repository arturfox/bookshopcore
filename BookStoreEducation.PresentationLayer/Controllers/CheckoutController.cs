﻿using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStoreEducation.PresentationLayer.Controllers
{
    [Authorize(Roles = "Client")]
    [ApiController]
    [Route("[controller]")]
    public class CheckoutController : ControllerBase
    {
        private readonly ICheckoutService _checkoutService;

        public CheckoutController(ICheckoutService checkoutService)
        {
            _checkoutService = checkoutService;
        }

        [HttpGet("create-session")]
        public async Task<ActionResult<ResponseModel>> CreateCheckoutSession()
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _checkoutService.CreateCheckoutSession(userId.Value);
            return Ok(result);
        }

        [HttpGet("create-session-for-order")]
        public async Task<ActionResult<ResponseModel>> CreateCheckoutSession(long orderId)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _checkoutService.CreateCheckoutSession(userId.Value, orderId);
            return Ok(result);
        }
    }
}