﻿using BookStoreEducation.BusinessLogicLayer.Configurations;
using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Account.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.Account.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using BookStoreEducation.Entities.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace BookStoreEducation.PresentationLayer.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly IAccountService _accountService;
        private readonly ICartService _cartService;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IOptions<AppSettings> _appSettings;

        public AccountController(IAccountService accountService,
            UserManager<ApplicationUser> userManager,
            IOptions<AppSettings> appSettings, 
            ICartService cartService)
        {
            _accountService = accountService;
            _userManager = userManager;
            _appSettings = appSettings;
            _cartService = cartService;
        }

        [AllowAnonymous]
        [HttpPost("CreateAccount")]
        public async Task<ActionResult<ResponseModel>> CreateAccount(CreateAccountModel accountModel)
        {
            var result = await _accountService.CreateAccount(accountModel);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpGet("EmailConfirmation")]
        public async Task<IActionResult> EmailConfirmation([FromQuery] string email, string token)
        {
            if (String.IsNullOrWhiteSpace(email) || String.IsNullOrWhiteSpace(token))
            {
                return Ok("Invalid link");
            }
            ApplicationUser user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return Ok("Invalid link");
            }
            IdentityResult result = await _accountService.ConfirmEmail(user, token);
            if (!result.Succeeded)
            {
                return Ok("Invalid link");
            }

            await _cartService.AddCart(user.Id);

            var loginResult = await _accountService.Login(user);

            //todo: encrypt token & refreshToken in route
            return Redirect($"{_appSettings.Value.FrontedUrl}login?token={loginResult.Token}&refreshtoken={loginResult.RefreshToken}");
        }

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<ActionResult<ResponseModel<LoginResultModel>>> Login(LoginModel loginModel)
        {
            var result = await _accountService.Login(loginModel);
            return Ok(result);
        }


        //todo: add token blacklist logic (cache, etc)
        [HttpPost("Logout")]
        public async Task<ActionResult<ResponseModel>> Logout()
        {
            var result = new ResponseModel();
            result.IsSucceed = true;

            return Ok(result);
        }


        [AllowAnonymous]
        [HttpPost("token/refresh")]
        public async Task<ActionResult<ResponseModel<LoginResultModel>>> RefreshToken(RefreshTokenModel refreshToken)
        {
            var result = await _accountService.RefreshToken(refreshToken);
            return Ok(result);
        }

        [AllowAnonymous]
        [HttpPost("recoverPassword")]
        public async Task<ActionResult<ResponseModel>> RecoverPassword(RecoverPasswordModel model)
        {
            var result = await _accountService.RecoverPassword(model);
            return Ok(result);
        }

        [Authorize(Roles = "Client")]
        [HttpPost("changePassword")]
        public async Task<ActionResult<ResponseModel>> ChangePassword(ChangePasswordModel model)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _accountService.ChangePassword(model, userId.Value);
            return Ok(result);
        }

        [Authorize]
        [HttpGet("profile")]
        public async Task<ActionResult<ResponseModel<ProfileModel>>> GetProfile()
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _accountService.GetProfile(userId.Value);
            return Ok(result);
        }

        [Authorize]
        [HttpPost("profile/update")]
        public async Task<ActionResult<ResponseModel>> UpdateProfile(UpdateProfileModel profileModel)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _accountService.UpdateProfile(profileModel, userId.Value);
            return Ok(result);
        }
    }
}