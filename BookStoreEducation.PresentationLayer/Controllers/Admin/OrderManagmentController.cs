﻿using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.OrderManagment.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static BookStoreEducation.BusinessLogicLayer.Common.Constants.Constants;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.PresentationLayer.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    [ApiController]
    [Route("[controller]")]
    public class OrderManagmentController : ControllerBase
    {
        private readonly IOrderManagmentService _orderManagmentService;

        public OrderManagmentController(IOrderManagmentService orderManagmentService)
        {
            _orderManagmentService = orderManagmentService;
        }

        [HttpGet("get")]
        public async Task<ActionResult<ResponseModel<OrdersModel>>> GetOrders(string orderStates,
           OrderManagmentSortType sortBy = OrderManagmentSortType.NumberDesc, int takeItems = Pagination.OrderManagment.ItemsCountPerPage, int page = 0)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _orderManagmentService.GetOrders(orderStates, sortBy, takeItems, page);
            return Ok(result);
        }
    }
}
