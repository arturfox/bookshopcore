﻿using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.UserManagment.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces;
using BookStoreEducation.Entities.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static BookStoreEducation.BusinessLogicLayer.Common.Constants.Constants;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.PresentationLayer.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    [ApiController]
    [Route("[controller]")]
    public class UserManagmentController : ControllerBase
    {
        private readonly IUserManagmentService _userManagmentService;

        public UserManagmentController(IUserManagmentService userManagmentService)
        {
            _userManagmentService = userManagmentService;
        }

        [HttpGet("get")]
        public async Task<ActionResult<ResponseModel<UsersModel>>> GetUsers(string searchQuery, string acсountStates, 
            UserManagmentSortType sortBy = UserManagmentSortType.UserNameAsc, int takeItems = Pagination.UserManagment.ItemsCountPerPage, int page = 0)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _userManagmentService.GetUsers(searchQuery, acсountStates, sortBy, takeItems, page); 
            return Ok(result);
        }

        [HttpPost("remove/{userId}")]
        public async Task<ActionResult<ResponseModel>> RemoveUser(long userId)
        {
            long? currentUserId = User.Identity.GetUserId();
            if (!currentUserId.HasValue)
            {
                return BadRequest();
            }

            var result = await _userManagmentService.RemoveUser(userId);
            return Ok(result);
        }

        [HttpPost("suspend/{userId}")]
        public async Task<ActionResult<ResponseModel>> SuspendUserAccount(long userId)
        {
            long? currentUserId = User.Identity.GetUserId();
            if (!currentUserId.HasValue)
            {
                return BadRequest();
            }

            var result = await _userManagmentService.UpdateUserAccountStatus(userId, AccountStatus.Suspended);
            return Ok(result);
        }

        [HttpPost("activate/{userId}")]
        public async Task<ActionResult<ResponseModel>> ActivateUserAccount(long userId)
        {
            long? currentUserId = User.Identity.GetUserId();
            if (!currentUserId.HasValue)
            {
                return BadRequest();
            }

            var result = await _userManagmentService.UpdateUserAccountStatus(userId, AccountStatus.Active);
            return Ok(result);
        }

        [HttpPost("update")]
        public async Task<ActionResult<ResponseModel>> UpdateUserAccount(UpdateUserModel accountModel)
        {
            long? currentUserId = User.Identity.GetUserId();
            if (!currentUserId.HasValue)
            {
                return BadRequest();
            }

            var result = await _userManagmentService.UpdateUser(accountModel);
            return Ok(result);
        }
    }
}
