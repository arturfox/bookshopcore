﻿using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.AuthorManagment.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static BookStoreEducation.BusinessLogicLayer.Common.Constants.Constants;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.PresentationLayer.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    [ApiController]
    [Route("[controller]")]
    public class AuthorManagmentController : ControllerBase
    {
        private readonly IAuthorManagmentService _authorManagmentService;

        public AuthorManagmentController(IAuthorManagmentService authorManagmentService)
        {
            _authorManagmentService = authorManagmentService;
        }

        [HttpGet("get")]
        public async Task<ActionResult<ResponseModel<AuthorsModel>>> GetList(AuthorManagmentSortType sortType = AuthorManagmentSortType.NumberDesc,
            int takeItems = Pagination.AuthorManagment.ItemsCountPerPage, int page = 0)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _authorManagmentService.GetAuthors(sortType, takeItems, page);
            return Ok(result);
        }

        [HttpGet("get/all")]
        public async Task<ActionResult<ResponseModel<AllAuthorsModel>>> GetAll()
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _authorManagmentService.GetAllAuthors();
            return Ok(result);
        }

        [HttpPost("add")]
        public async Task<ActionResult<ResponseModel>> AddAuthor(AddAuthorModel authorModel)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _authorManagmentService.AddAuthor(authorModel);
            return Ok(result);
        }

        [HttpPost("remove/{authorId}")]
        public async Task<ActionResult<ResponseModel>> RemoveAuthor(long authorId)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _authorManagmentService.RemoveAuthor(authorId);
            return Ok(result);
        }

        [HttpPost("update")]
        public async Task<ActionResult<ResponseModel>> UpdateAuthor(UpdateAuthorModel authorModel)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _authorManagmentService.UpdateAuthor(authorModel);
            return Ok(result);
        }
    }
}
