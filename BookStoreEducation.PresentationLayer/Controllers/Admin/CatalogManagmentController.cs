﻿using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.CatalogManagment.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Admin.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using static BookStoreEducation.BusinessLogicLayer.Common.Constants.Constants;
using static BookStoreEducation.Shared.Enums.Enums;

namespace BookStoreEducation.PresentationLayer.Controllers.Admin
{
    [Authorize(Roles = "Admin")]
    [ApiController]
    [Route("[controller]")]
    public class CatalogManagmentController : ControllerBase
    {
        private readonly ICatalogManagmentService _catalogManagmentService;

        public CatalogManagmentController(ICatalogManagmentService catalogManagmentService)
        {
            _catalogManagmentService = catalogManagmentService;
        }

        [HttpGet("get")]
        public async Task<ActionResult<ResponseModel<CatalogModel>>> GetList(string productCategories, CatalogManagmentSortType sortType = CatalogManagmentSortType.NumberDesc, 
            int takeItems = Pagination.CatalogManagment.ItemsCountPerPage, int page = 0)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _catalogManagmentService.GetCatalog(productCategories, sortType, takeItems, page);
            return Ok(result);
        }

        [HttpPost("add")]
        public async Task<ActionResult<ResponseModel>> AddCatalogItem(AddCatalogItemModel addCatalogItem)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _catalogManagmentService.AddCatalogItem(addCatalogItem);
            return Ok(result);
        }

        [HttpPost("remove/{productId}")]
        public async Task<ActionResult<ResponseModel>> RemoveCatalogItem(long productId)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _catalogManagmentService.RemoveItem(productId);
            return Ok(result);
        }

        [HttpPost("update")]
        public async Task<ActionResult<ResponseModel>> UpdateCatalogItem(UpdateCatalogItemModel itemModel)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _catalogManagmentService.UpdateCatalogItem(itemModel);
            return Ok(result);
        }
    }
}
