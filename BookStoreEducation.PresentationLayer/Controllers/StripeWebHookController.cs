﻿using BookStoreEducation.BusinessLogicLayer.Common.Constants;
using BookStoreEducation.BusinessLogicLayer.Configurations;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Stripe;
using System.IO;
using System.Threading.Tasks;

namespace BookStoreEducation.PresentationLayer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class StripeWebHookController : ControllerBase
    {
        private readonly ICheckoutService _checkoutService;
        private readonly IOptions<StripeOptions> _stripeOptions;

        public StripeWebHookController(ICheckoutService checkoutService, 
            IOptions<StripeOptions> stripeOptions)
        {
            _checkoutService = checkoutService;
            _stripeOptions = stripeOptions;
        }

        [AllowAnonymous]
        [HttpPost("Notify")]
        public async Task<IActionResult> HandleCheckoutWebhook()
        {
            var json = await new StreamReader(HttpContext.Request.Body).ReadToEndAsync();

            try
            {
                var stripeEvent = EventUtility.ConstructEvent(
                  json,
                  Request.Headers[Constants.Payments.Stripe.StripeSignatureHeader],
                  _stripeOptions.Value.WebHookSecretKey
                );

                if (stripeEvent.Type == Events.CheckoutSessionCompleted)
                {
                    var session = stripeEvent.Data.Object as Stripe.Checkout.Session;
                    await _checkoutService.FulfillOrder(session);
                }

                return Ok();
            }
            catch (StripeException)
            {
                return BadRequest();
            }
        }
    }
}