﻿using BookStoreEducation.BusinessLogicLayer.Extensions;
using BookStoreEducation.BusinessLogicLayer.Models;
using BookStoreEducation.BusinessLogicLayer.Models.Cart.Requests;
using BookStoreEducation.BusinessLogicLayer.Models.Cart.Responses;
using BookStoreEducation.BusinessLogicLayer.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStoreEducation.PresentationLayer.Controllers
{
    [Authorize(Roles = "Client")]
    [ApiController]
    [Route("[controller]")]
    public class CartController : ControllerBase
    {
        private readonly ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        [HttpGet("getCart")]
        public async Task<ActionResult<ResponseModel<CartModel>>> GetCart()
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _cartService.GetCart(userId.Value);
            return Ok(result);
        }


        [HttpPost("addPosition")]
        public async Task<ActionResult<ResponseModel>> AddPosition(AddPositionModel model)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _cartService.AddPosition(model, userId.Value);
            return Ok(result);
        }

        [HttpPost("removePosition")]
        public async Task<ActionResult<ResponseModel<CartModel>>> RemovePosition(RemovePositionModel model)
        {
            long? userId = User.Identity.GetUserId();
            if (!userId.HasValue)
            {
                return BadRequest();
            }

            var result = await _cartService.RemovePosition(model, userId.Value);
            return Ok(result);
        }
    }
}
