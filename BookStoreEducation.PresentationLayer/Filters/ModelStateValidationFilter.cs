﻿using BookStoreEducation.BusinessLogicLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Net;

namespace BookStoreEducation.PresentationLayer.Filters
{
    public class ModelStateValidationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var response = new ResponseModel()
                {
                    IsSucceed = false,
                    StatusCode = (int)HttpStatusCode.UnprocessableEntity
                };
                response.Errors.AddRange(context.ModelState.Values.SelectMany(x => x.Errors)
                                                                  .Select(x => x.ErrorMessage));

                context.Result = new OkObjectResult(response);
                return;
            }
        }
    }
}
